import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidian/screens/dashboard.dart';
import 'package:sidian/screens/time_sheet.dart';
import 'package:sidian/globals.dart' as globals;

makeAPICALL(String title,String startTime,String endTime,String currentDate,String location,String agenda,BuildContext context) async{
  String url= globals.BASE_URL;
  String resp="",validation="";
  var jsonObj;

  SharedPreferences prefs=await SharedPreferences.getInstance();
  String token=prefs.getString("token")??"";
  Map map={
    "username": "esb",
    "password": "esb@ao2021",
    "msgType": "1200",
    "processingCode": "999000",
    "serviceCode": "400000",
    "channel": "APP",
    "location": location,
    "agenda": agenda,
    "title": title,
    "startTime": startTime,
    "endTime": endTime,
    "date": currentDate,
    "token" : token
  };

  resp = (await globals.apiRequest(globals.BASE_URL, map));
  print(resp);
  jsonObj = json.decode(resp);
  //response = jsonObj['response'];
  validation = jsonObj['validation'];

  Navigator.pop(context);
  Fluttertoast.showToast(
      msg: validation,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      fontSize: 16,
      textColor: Colors.white,
      timeInSecForIosWeb: 1
  );
  if(validation=="pass"){
    prefs.setString("location", location);
    prefs.setString("agenda", agenda);
    prefs.setString("title", title);
    prefs.setString("startTime", startTime);
    prefs.setString("endTime", endTime);
    prefs.setString("currentDate", currentDate);

   // Navigator.pop(context);
   /* Navigator.push(context, MaterialPageRoute(builder: (context)=>TimeSheet(
      location: location,
      title: title,
      startTime: startTime,
      endTime: endTime,
      currentDate: currentDate,
      agenda: agenda,
    )));*/
    Navigator.push(context, MaterialPageRoute(builder: (context)=>TimeSheet(
        location: location,
        title: title,
        startTime: startTime,
        endTime: endTime,
        currentDate: currentDate,
        agenda: agenda)));
  }else{}
}

@override
showLoaderDialog(BuildContext context) {
  AlertDialog alert = AlertDialog(
    content: new Row(
      children: [
        CircularProgressIndicator(),
        Container(
            margin: EdgeInsets.only(left: 7), child: Text("Saving...")),
      ],
    ),
  );
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

class TimePopScreen extends StatefulWidget {
  const TimePopScreen({Key? key}) : super(key: key);

  @override
  State<TimePopScreen> createState() => _TimePopScreenState();
}

class _TimePopScreenState extends State<TimePopScreen> {
  TimeOfDay startTime=TimeOfDay.now();
  TimeOfDay endTime=TimeOfDay.now();
  DateTime currentDate=DateTime.now();
  bool _isTimeSelected=false;
  bool _isDateSelected=false;

  TextEditingController _titleEditingController=TextEditingController();
  TextEditingController _locationEditingController=TextEditingController();
  TextEditingController _agendaEditingController=TextEditingController();


  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? pickedTime=await showTimePicker(
      context: context,
      initialTime: startTime,
    );

    if(pickedTime!=null && pickedTime!=startTime){
      setState(() {
        startTime=pickedTime;
        _isTimeSelected=true;
      });
    }
  }

  Future<void> _selectEndTime(BuildContext context) async {
    final TimeOfDay? pickedTime=await showTimePicker(
      context: context,
      initialTime: endTime,
    );

    if(pickedTime!=null && pickedTime!=endTime){
      setState(() {
        endTime=pickedTime;
        _isTimeSelected=true;
      });
    }
  }
  //for datepicker
  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate=await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime(2022),
        lastDate: DateTime(2040),
        useRootNavigator: true
    );

    if(pickedDate != null && pickedDate != currentDate){
      setState(() {
        currentDate=pickedDate;
        _isDateSelected=true;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:   AppBar(
        title: Text("Create New TimeSheet"),
        backgroundColor: Colors.black,
      ),
      body: Center(
        child: Container(
          child: ListView.builder(
              itemCount: 1,
              itemBuilder: (context,index){
            return Container(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('New Time Sheet',style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                    ],
                  ),
                  Container(
                    child: TextField(
                      //enabled: false,
                       controller: _titleEditingController,
                      //scannedText;
                      decoration: InputDecoration(
                        hintText: "Title",
                        labelText: "Title",
                        border: OutlineInputBorder(),
                      ),
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(26)
                    ),
                  ),
                  SizedBox(height:5,),
                  Card(
                    child: ListTile(
                      title: _isTimeSelected ? Text('${startTime.format(context).toString()}'):Text("Start time"),
                      trailing: Icon(Icons.edit),
                      onTap: (){
                       _selectTime(context);
                      },
                    ),
                  ),
                  SizedBox(height: 5,),
                  //Text(scannedText),
                  Card(
                    child: ListTile(
                      title: _isTimeSelected ? Text('${endTime.format(context).toString()}'):Text("End time"),
                      trailing: Icon(Icons.edit),
                      onTap: (){
                        _selectEndTime(context);
                      },
                    ),
                  ),
                  SizedBox(height: 5,),
                  Card(
                    child: ListTile(
                      title:_isDateSelected ? Text('${currentDate.toString()}'):Text('Date'),
                      trailing: Icon(Icons.edit),
                      onTap: (){
                        _selectDate(context);
                      },
                    ),
                  ),
                  SizedBox(height: 5,),
                  Container(
                    child: TextField(
                      controller: _locationEditingController,
                      decoration: InputDecoration(
                        hintText: "Location",
                        labelText: "Location",
                        border: OutlineInputBorder(),
                      ),
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(26)
                    ),
                  ),
                  SizedBox(height: 5,),
                  Container(
                    child: TextField(
                      // maxLines: null,
                      minLines: 2,
                      maxLines: 5,
                      keyboardType: TextInputType.multiline,
                      controller: _agendaEditingController,
                      decoration: InputDecoration(
                        hintText: "Agenda",
                        labelText: "Agenda",
                        border: OutlineInputBorder(),
                      ),
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(26)
                    ),
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      FlatButton(onPressed: (){
                        Navigator.pop(context);
                      }, child: Text("Cancel")),
                      SizedBox(width: 5,),
                      FlatButton(onPressed: (){
                        showLoaderDialog(context);
                        makeAPICALL(_titleEditingController.text.trim(),
                            startTime.format(context).toString(),
                            endTime.format(context).toString(),
                            currentDate.toString(),
                            _locationEditingController.text.trim(),
                            _agendaEditingController.text.trim(), context);
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>TimeSheet(
                          location: _locationEditingController.text,
                          title: _titleEditingController.text,
                          startTime: startTime.format(context).toString(),
                          endTime: currentDate.toString(),
                          currentDate: currentDate.toString(),
                          agenda: _agendaEditingController.text,
                        )));
                      }, child: Text("Save")),
                    ],
                  )
                ],
              )
            );
          }),
        ),
      ),
    );
  }
}
