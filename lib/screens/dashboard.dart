import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidian/globals.dart' as globals;
import 'package:sidian/screens/accounts.dart';
import 'package:sidian/screens/menu_drawer.dart';

class DashBoard extends StatefulWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final urlSky="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzDEhTgdRdxlQfAfFkrJHodjTYXQActHFt3ZiPueg5B9j3XBvGdJezsiHZgsug6zo8Ns0&usqp=CAU";
    var titleList=["Retail Products","Corporate Products"];
    var desListCount=['Count: 300','Count: 100 '];
    double _value=0.7;
    return Stack(
      children: [
        Scaffold(
          appBar: AppBar(
            title: Text("Dashboard"),
            actions: [
              IconButton(onPressed: (){}, icon: Icon(Icons.more_vert)),
            ],
            backgroundColor: Colors.black,
          ),
          drawer: MenuDrawer(),
          body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/dfa.png'),
                  fit: BoxFit.cover,
                )
            ),
            child: ListView.builder(
                itemCount: 2,
                padding: const EdgeInsets.all(16),
                itemBuilder: (context, index) {
                  return ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Container(
                      margin: const EdgeInsets.only(bottom: 10),
                      color:Colors.white,
                      padding: const EdgeInsets.all(8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(titleList[index],style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                              ),
                              SizedBox(height: 10,),
                              Container(
                                child: Row(
                                  children: [
                                    Text('Total: 500',style: TextStyle(fontSize: 10,fontWeight: FontWeight.bold),),
                                    SizedBox(width: 10,),
                                    Text(desListCount[index],style: TextStyle(fontWeight: FontWeight.bold,fontSize: 10),)
                                  ],
                                ),
                              ),
                              SizedBox(height: 10,),
                              Slider(value: _value,activeColor: Colors.yellowAccent, onChanged: (newValue){
                              })
                            ],
                          ),
                          ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: Image.network(
                              urlSky,
                              width: 80,
                              height: 80,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                }),
          ),
        )
      ],
    );
  }
}
