import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:sidian/screens/login_page.dart';
import 'package:sidian/globals.dart' as globals;
import 'package:sidian/screens/login.dart';

import '../text_style.dart';

ShowLoaderDialog(BuildContext context){
  AlertDialog alert=AlertDialog(
    content: new Row(
      children: [
        CircularProgressIndicator(),
       Container(margin: EdgeInsets.only(left: 7),child: Text("Loading...."),)
      ],
    ),
  );
  showDialog(
     barrierDismissible: false,
      context: context,
      builder: (BuildContext context){
        return alert;
      });
}

makeAPICALL(String email,BuildContext context) async{
  ShowLoaderDialog(context);
  String url=globals.BASE_URL;
  String resp="";
  String responseDescription="";
  String response="";
  var jsonObj;

  SharedPreferences prefs=await SharedPreferences.getInstance();
  //String token=prefs.getString("token")??"";
 // String password=prefs.getString("old_password")??"";
  Map map={
    "username": "esb",
    "password": "esb@ao2021",
    "msgType": "1200",
    "processingCode": "110500",
    "dfaChannel": "web",
    "email": email,
    "timestamp": "18215d2edd1"
  };

  resp = (await globals.apiRequest(globals.BASE_URL, map));
  jsonObj=json.decode(resp);
  response=jsonObj["response"];
  responseDescription=jsonObj["responseDescription"];
  Navigator.pop(context);

  Fluttertoast.showToast(
    msg: responseDescription,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
    timeInSecForIosWeb: 1,
    textColor: Colors.white,
    fontSize: 16.0
  );

  if(response=="000"){
    prefs.setString("old_password", "");
    prefs.setString("email", email);
    Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage()));
  }else{

  }
}

class RecoveryPage extends StatefulWidget {
   RecoveryPage({Key? key}) : super(key: key);

  @override
  State<RecoveryPage> createState() => _RecoveryPageState();
}

class _RecoveryPageState extends State<RecoveryPage> {
  TextEditingController emailController=TextEditingController();

  //TextEditingController _passwordControler=TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
          padding: EdgeInsets.all(0),
        child: ListView(
          children: [
            Card(
              color: Colors.redAccent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10.0),
                    child: Text(
                      "Forget Password ",
                      style: TextStyle(fontSize: 20.0,color: Colors.white70),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: TextField(
                      style: TextStyle(color: Colors.black),
                      controller: emailController,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: new BorderSide(color: Colors.black)
                        ),
                        labelStyle: TextStyle(color: Colors.black),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                          borderSide: new BorderSide(color: Colors.lightGreen)
                        ),
                        labelText: "Email",
                        prefixIcon: const Icon(
                          Icons.email_outlined,
                          color: Colors.black,
                        )
                      ),
                    ),
                  ),
                  Container(
                    height: 70,
                    width: 200,
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: FlatButton(
                        color: Colors.yellow.shade200,
                        textColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(
                            color: Colors.white,width: 2
                          )
                        ),
                        child: Text("Recover"),
                        onPressed: (){
                          print(emailController.text);

                          emailController.text.trim().length > 0 ? makeAPICALL(emailController.text.trim(), context) :
                              Fluttertoast.showToast(
                                msg: "Email is required",
                                toastLength: Toast.LENGTH_LONG,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                                textColor: Colors.white,
                                fontSize: 16.0
                              );
                        },
                    ),
                  ),
                  FlatButton(
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage()));
                      },
                      child: Text("Back to Login"),
                      color: Colors.white70,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


