import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sidian/screens/accounts.dart';
//import 'package:sidian/screens/dashboardtext.dart';
import 'package:sidian/screens/loans.dart';
import 'package:sidian/screens/login.dart';
//import 'package:sidian/screens/radio_button_page_account.dart';
import 'package:sidian/screens/time_pop_screen.dart';
//import 'package:sidian/screens/loans_text.dart';
//import 'package:sidian/screens/time_sheet.dart';
import 'package:sidian/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidian/screens/dashboard.dart';


enum ganderGroup{Retail,Corporate}
class MenuDrawer extends StatefulWidget {
   const MenuDrawer({Key? key}) : super(key: key);

  @override
  State<MenuDrawer> createState() => _MenuDrawerState();
}

class _MenuDrawerState extends State<MenuDrawer> {
  late String userEmail=globals.userEmail;

 getEmailOfUser() async{
   SharedPreferences prefs=await SharedPreferences.getInstance();
   String email=prefs.getString("email") ?? "";
   setState(() {
     userEmail=email;
   });
 }

  //late ganderGroup _value;
  @override
  void initState(){
     super.initState();
    // _getValidatedDetail(userEmail);
    getEmailOfUser();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
   // _getValidatedDetail(userEmail);
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width*0.8,
      child: Drawer(
        child: ListView.builder(
            itemCount: 1,
            itemBuilder: (context,index){
          return Container(
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(20),
                  color: Colors.lightGreen,
                  child: Center(
                    child: Column(
                      children: [
                        Container(
                          width: 50,
                          height: 100,
                          margin: EdgeInsets.only(top: 30,bottom: 10),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.blue,
                          ),
                        ),
                        Text('Abdirahman Abdi',style: TextStyle(fontSize: 20,color: Colors.white),),
                        SizedBox(height: 5,),
                        Text(userEmail,style: TextStyle(fontSize: 20,color: Colors.white),),
                      ],
                    ),
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.dashboard),
                  title: Text('Dashboard',style: TextStyle(fontSize: 18),),
                  onTap: (){
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>DashBoard()));

                  },
                ),
                ListTile(
                  leading: Icon(Icons.account_circle),
                  title: Text('Accounts',style: TextStyle(fontSize: 18),),
                  onTap: (){
                    Navigator.pop(context);
                   Navigator.push(context, MaterialPageRoute(builder: (context)=>AccountsPage()));

                  },
                ),
                ListTile(
                  leading: Icon(Icons.money),
                  title: Text('Loans',style: TextStyle(fontSize: 18),),
                  onTap: (){
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>LoansPage()));
                  },
                ),
                ListTile(
                  leading: Icon(Icons.timer),
                  title: Text('Time Sheet',style: TextStyle(fontSize: 18),),
                  onTap: (){
                    Navigator.pop(context);
                     Navigator.push(context, MaterialPageRoute(builder: (context)=>TimePopScreen()));
                  },
                ),
                ListTile(
                  leading: Icon(Icons.help),
                  title: Text('Help',style: TextStyle(fontSize: 18),),
                  onTap: null,
                ),
                ListTile(
                  leading: Icon(Icons.one_k_plus),
                  title: Text('FAQs',style: TextStyle(fontSize: 18),),
                  onTap: null,
                ),
                ListTile(
                  leading: Icon(Icons.arrow_back),
                  title: Text('Logout',style: TextStyle(fontSize: 18),),
                  onTap: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage()));
                  },
                ),
              ],
            ),
          );
        }),
      ),
    );
  }
}

