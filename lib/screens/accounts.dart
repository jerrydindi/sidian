import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sidian/globals.dart' as globals;
import 'package:sidian/screens/dashboard.dart';
import '../text_style.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:developer';
import 'package:fluttertoast/fluttertoast.dart';
import 'menu_drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../model/retail_model.dart';

//making API call for MandateTabShow
makeAPICALLMANDATE(String signature,BuildContext context) async{
  String url=globals.BASE_URL;
  String resp="";
  String response="";
  String responseDescription="";
  var jsonObj;

  SharedPreferences prefs=await SharedPreferences.getInstance();
  String token=prefs.getString("token") ?? "";
  String agentId=prefs.getString("id") ?? "";
  String name=prefs.getString("name") ?? "";
  String gender=prefs.getString("gender") ?? "";
  String nationality=prefs.getString("nationality") ?? "";
  String phoneNumber=prefs.getString("phone_number") ?? "";
  String idNumber=prefs.getString("id_number") ?? "";
  String kra=prefs.getString("kra") ?? "";
  String employer=prefs.getString("employer") ?? "";
  String email=prefs.getString("email") ?? "";
  String city=prefs.getString("city") ?? "";
  String physicalAddress=prefs.getString("physical_address") ?? "";


  Map map={
    "username": "esb",
    "password": "esb@ao2021",
    "msgType": "1200",
    "processingCode": "999000",
    "serviceCode": "160000",
    "name":name,
    "gender":gender,
    "nationality":nationality,
    "phoneNumber":phoneNumber,
    "dob":"27-11-1995",
    "idNumber":idNumber,
    "tinNumber":kra,
    "employer":employer,
    "email":email,
    "city":city,
    "accountType":"retail",
    "agentId":agentId,
    "physicalAddress":physicalAddress,
    "signature":signature,
    "tag":"WOBIO",
    "token":token
  };

  resp=(await globals.apiRequest(globals.BASE_URL, map));
  print(resp);
  jsonObj=json.decode(resp);
  response=jsonObj["response"];
  responseDescription=jsonObj["responseDescription"];

  Fluttertoast.showToast(
      msg: responseDescription,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      textColor: Colors.white,
      fontSize: 16.0);
  if(response=="999"){

  }else if(response=="000"){
    prefs.setString("signature", signature);
    _tabController.index=0;
    //Navigator.push(context, MaterialPageRoute(builder: (context)=>DashBoard()));
    //_tabController;
    //Navigator.pop(context);
  }
}
//making API call for kyc
makeAPICALL(String name,String gender,String nationality,String phoneNumber, String idNumber,String kra,String employer,String email,
    String city,String physicalAddress,BuildContext context ) async{
  /*String url=globals.BASE_URL;
  String resp="";
  String response="";
  String responseDescription="";
  var jsonObj;*/

  SharedPreferences prefs=await SharedPreferences.getInstance();
  //String token=prefs.getString("token") ?? "";
 // String agentId=prefs.getString("id") ?? "";
  prefs.setString("name", name);
  prefs.setString("gender", gender);
  prefs.setString("nationality", nationality);
  prefs.setString("phone_number", phoneNumber);
  prefs.setString("id_number", idNumber);
  prefs.setString("kra", kra);
  prefs.setString("employer", employer);
  prefs.setString("email", email);
  prefs.setString("city", city);
  //prefs.setString("date", date);
  prefs.setString("physical_address", physicalAddress);
  _tabController.index=2;

  /*Map map={
    "username": "esb",
    "password": "esb@ao2021",
    "msgType": "1200",
    "processingCode": "999000",
    "serviceCode": "160000",
    "name":name,
    "gender":gender,
    "nationality":nationality,
    "phoneNumber":phoneNumber,
    "dob":"27-11-1995",
    "idNumber":idNumber,
    "tinNumber":kra,
    "employer":employer,
    "email":email,
    "city":city,
    "accountType":"retail",
    "agentId":agentId,
    "physicalAddress":physicalAddress,
    "signature":"svkjbkavlarjbkrbkafbvlkareboab osnboarbheonbra",
    "tag":"WOBIO",
    "token":token
  };*/
  /*resp=(await globals.apiRequest(globals.BASE_URL, map));
  print(resp);
  jsonObj=json.decode(resp);
  response=jsonObj["response"];
  responseDescription=jsonObj["responseDescription"];
  Navigator.pop(context);
  Fluttertoast.showToast(
      msg: responseDescription,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      textColor: Colors.white,
      fontSize: 16.0);*/
  /*if(response=="000"){
    prefs.setString("name", name);
    prefs.setString("gender", gender);
    prefs.setString("nationality", nationality);
    prefs.setString("phone_number", phoneNumber);
    prefs.setString("id_number", idNumber);
    prefs.setString("kra", kra);
    prefs.setString("employer", employer);
    prefs.setString("email", email);
    prefs.setString("city", city);
    //prefs.setString("date", date);
    prefs.setString("physical_address", physicalAddress);

    //Navigator.push(context, MaterialPageRoute(builder: (context)=>MandateTabShowCard()));
    _tabController.index=2;
  }else{}*/
}
Future<List<RetailModel>> makeAPICALLRETAIL() async{
  // String url=globals.BASE_URL;
  String resp="";
  String response="";
  String responseDescription="";
  var jsonObj;

  SharedPreferences prefs=await SharedPreferences.getInstance();
  String token=prefs.getString("token") ?? "";
  Map map={
    "username": "esb",
    "password": "esb@ao2021",
    "msgType": "1200",
    "processingCode": "999000",
    "serviceCode": "500000",
    "channel": "APP",
    "token": token
  };

  resp=(await globals.apiRequest(globals.BASE_URL, map));
  jsonObj=json.decode(resp);
  List<RetailModel> retailData=[];
  response=jsonObj["response"];
  responseDescription=jsonObj["responseDescription"];
  var dataList=jsonObj["data"];
  Fluttertoast.showToast(
      msg: responseDescription,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      textColor: Colors.white,
      fontSize: 16.0);
  if(response=="999"){}
  else if(response=="000"){
    prefs.setString("data__retail", response);
    for(var item in dataList){
      retailData.add(RetailModel.fromJson(item));
    }
    log(retailData.toString());
  }
  return retailData;
}
//request for corporate product to Accounts page
Future<List<RetailModel>> makeAPICALLRETAILFOR() async {
  String url = globals.BASE_URL;
  String resp = "";
  String response = "";
  String responseDescription = "";
  var jsonObj;

  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString("token") ?? "";
  Map map = {
    "username": "esb",
    "password": "esb@ao2021",
    "msgType": "1200",
    "processingCode": "999000",
    "serviceCode": "501000",
    "channel": "APP",
    "token" : token
  };

  resp = (await globals.apiRequest(globals.BASE_URL, map));
  print(resp);
  jsonObj = json.decode(resp);
  List<RetailModel> retailData = [];
  response = jsonObj["response"];
  responseDescription = jsonObj["responseDescription"];
  var dataList = jsonObj['data'];
  Fluttertoast.showToast(
      msg: responseDescription,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      textColor: Colors.white,
      fontSize: 16.0);
  if (response == "999") {
  } else if (response == "000") {
    prefs.setString("data__retail", response);
    for (var item in dataList) {
      retailData.add(RetailModel.fromJson(item));
    }
    log(retailData.toString());
    // jsonEncode(jsonObj['data']);
    //Navigator.push(context, MaterialPageRoute(builder: (context)=>AccountsPage()));
  }

  return retailData;
}

enum ganderGroup{Retail,Corporate}
late final TabController _tabController;

//MandateTabShow page
class MandateTabShowCard extends StatefulWidget {
  const MandateTabShowCard({Key? key}) : super(key: key);

  @override
  State<MandateTabShowCard> createState() => _MandateTabShowCardState();
}

class _MandateTabShowCardState extends State<MandateTabShowCard> {
  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Submitting...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  bool _validate=false;
  TextEditingController signatureEditingController=TextEditingController();
  bool textScanning=false;
  XFile? imageFile;
  String scannedText="";

  void getImage(ImageSource source) async{
    try{
      final pickedImage=await ImagePicker().pickImage(source: source);
      if(pickedImage != null){
        textScanning=true;
        imageFile=pickedImage;
        setState(() {
          getRecognisedText(pickedImage);
        });
      }
    } catch(e){
      textScanning=false;
      imageFile=null;
      scannedText="Error in capturing image text";
      setState(() {

      });
    }
  }

  void getRecognisedText(XFile image) async {
    final inputImage = InputImage.fromFilePath(image.path);
    final textDetector = GoogleMlKit.vision.textDetector();
    RecognisedText recognisedText = await textDetector.processImage(inputImage);
    await textDetector.close();
    scannedText = "";
    //var strArr;
    for (TextBlock block in recognisedText.blocks) {
      for (TextLine line in block.lines) {
        scannedText = scannedText + line.text + "\n";
        //strArr=scannedText;
      }
    }
    textScanning = false;

    setState(() {
      // _firstNameEditingController.text=strArr[2];
      // _idNumberEditingController.text=strArr[1];
      // _genderEditingController.text=strArr[3];
      //_dateOfBirthEditingController.text=strArr[4];
      // signatureEditingController.text=scannedText;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView.builder(
            itemCount: 1,
            itemBuilder: (context,index){
              return Container(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Signatory',style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                        SizedBox(width: 20,),
                        IconButton(onPressed: (){
                          getImage(ImageSource.camera);
                        },
                          icon: Icon(Icons.document_scanner,color: Colors.black,),)
                      ],
                    ),
                    Container(
                      child: TextField(
                        //enabled: false,
                        controller: signatureEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Signature",
                          labelText: "Signature",
                          border: OutlineInputBorder(),
                          errorText: _validate ? "Please fill in the field its required":null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.lightGreen,
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: FlatButton(
                              onPressed:(){
                                //Navigator.pop(context);
                                _tabController.index=1;
                              },
                              child:Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 2.0),
                                  child: Text('Previous',style: lBodyStyle,)),),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.lightGreen,
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: FlatButton(
                              onPressed:(){
                                setState(() {
                                  signatureEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                });
                                if(signatureEditingController.text.trim().isNotEmpty){
                                  //showLoaderDialog(context);
                                  makeAPICALLMANDATE(signatureEditingController.text.trim(), context);
                                }
                                //showLoaderDialog(context);
                               /* if(signatureEditingController==null){
                                  Text("Please fill in signature");
                                }else{
                                  showLoaderDialog(context);
                                  makeAPICALLMANDATE(signatureEditingController.text.trim(), context);
                                }*/
                                // Navigator.push(context, MaterialPageRoute(builder: (context)=>DashBoard()));
                              },
                              child:Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 2.0),
                                  child: Text('Submit',style: lBodyStyle,)),),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              );
            }),
      ),
    );
  }
}
//KYC page
class KycTabShowCard extends StatefulWidget {
  const KycTabShowCard( {Key? key}) : super(key: key);

  @override
  State<KycTabShowCard> createState() => _KycTabShowCardState();
}

//Class to denie scrolling of Tabcontroller Manually
class ReadOnlyTabBar extends StatelessWidget implements PreferredSizeWidget {
  final TabBar child;

  const ReadOnlyTabBar({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(child: child);
  }

  @override
  Size get preferredSize => child.preferredSize;
}

class _KycTabShowCardState extends State<KycTabShowCard> {

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Saving...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  showForEmptyTextDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Fill the black...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  DateTime currentDate=DateTime.now();
  bool _isDateSelected=false;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate=await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime(2022),
        lastDate: DateTime(2040),
        useRootNavigator: true
    );

    if(pickedDate != null && pickedDate != currentDate){
      setState(() {
        currentDate=pickedDate;
        _isDateSelected=true;
      });
    }
  }
  bool _validate=false;
  TextEditingController nameEditingController=TextEditingController();
  TextEditingController genderEditingController=TextEditingController();
  TextEditingController nationalityEditingController=TextEditingController();
  TextEditingController phoneNumberEditingController=TextEditingController();
  TextEditingController idNumberEditingController=TextEditingController();
  TextEditingController kraEditingController=TextEditingController();
  //TextEditingController _dateOfBirthEditingController=TextEditingController();
  TextEditingController employerEditingController=TextEditingController();

  //for the contact details
  TextEditingController emailEditingController=TextEditingController();
  TextEditingController cityEditingController=TextEditingController();
  TextEditingController physicalAddressEditingController=TextEditingController();
  bool textScanning = false;

  XFile? imageFile;

  String scannedText = "";


  void getImage(ImageSource source) async {
    try {
      final pickedImage = await ImagePicker().pickImage(source: source);
      if (pickedImage != null) {
        textScanning = true;
        imageFile = pickedImage;
        setState(() {
          getRecognisedText(pickedImage);
        });
      }
    } catch (e) {
      textScanning = false;
      imageFile = null;
      scannedText = "Error occurred while scanning";
      setState(() {});
    }
  }

  void getRecognisedText(XFile image) async {
    final inputImage = InputImage.fromFilePath(image.path);
    final textDetector = GoogleMlKit.vision.textDetector();
    RecognisedText recognisedText = await textDetector.processImage(inputImage);
    await textDetector.close();
    scannedText = "";
    var strArr;
    for (TextBlock block in recognisedText.blocks) {
      for (TextLine line in block.lines) {
        scannedText = scannedText + line.text + "\n";
        strArr=scannedText;
      }
    }
    textScanning = false;

    setState(() {
      nameEditingController.text=strArr[3];
      genderEditingController.text=strArr[4];
      nationalityEditingController.text=strArr[1];
      idNumberEditingController.text=strArr[2];

      // _firstNameEditingController.text=strArr[2];
      // _idNumberEditingController.text=strArr[1];
      // _genderEditingController.text=strArr[3];
      //_dateOfBirthEditingController.text=strArr[4];
    });
  }

  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        child: ListView.builder(
            itemCount: 1,
            itemBuilder: (context,index){
              return Container(
                padding: EdgeInsets.only(left: 5.0,right: 5.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Basic Details',style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                        SizedBox(width: 20,),
                        IconButton(onPressed: (){
                          getImage(ImageSource.camera);
                        },
                          icon: Icon(Icons.document_scanner,color: Colors.black,),)
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: nameEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Name",
                          labelText: "Name",
                          border: OutlineInputBorder(),
                          errorText: _validate ? "Please fill in the field":null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: genderEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Gender",
                          labelText: "Gender",
                          border: OutlineInputBorder(),
                          errorText: _validate ? "Please fill in the field":null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: nationalityEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Nationality",
                          labelText: "Nationality",
                          border: OutlineInputBorder(),
                          errorText: _validate ? "Please fill in the field":null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: phoneNumberEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Phone Number",
                          labelText: "Phone Number",
                          border: OutlineInputBorder(),
                          errorText: _validate ? "Please fill in the field":null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: idNumberEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Id Number",
                          labelText: "Id Number",
                          border: OutlineInputBorder(),
                          errorText: _validate ? "PLease fill in the field":null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: kraEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Kra pin",
                          labelText: "Kra pin",
                          border: OutlineInputBorder(),
                          errorText: _validate ? "Please fill in the field":null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: employerEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Employer",
                          labelText: "Employer",
                          border: OutlineInputBorder(),
                          errorText: _validate ? "Please fill in the field":null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: emailEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Email",
                          labelText: "Email",
                          border: OutlineInputBorder(),
                          errorText: _validate ? "Please fill in the field":null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: cityEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "City",
                          labelText: "City",
                          border: OutlineInputBorder(),
                          errorText: _validate ? "Please fill in the field":null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: physicalAddressEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Physical Address",
                          labelText: "Physical Address",
                          border: OutlineInputBorder(),
                          errorText: _validate ? "Please fill in the field":null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.lightGreen,
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: FlatButton(
                              onPressed:(){
                                //Navigator.pop(context);
                                _tabController.index=0;
                              },
                              child:Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 0.0),
                                  child: Text('Previous',style: lBodyStyle,)),),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.lightGreen,
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: FlatButton(
                              onPressed:(){
                                setState(() {
                                  nameEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  genderEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  nationalityEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  phoneNumberEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  idNumberEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  kraEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  employerEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  emailEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  cityEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  physicalAddressEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                });
                                if(nameEditingController.text.trim().isNotEmpty &&
                                    genderEditingController.text.trim().isNotEmpty &&
                                    nationalityEditingController.text.trim().isNotEmpty &&
                                    phoneNumberEditingController.text.trim().isNotEmpty &&
                                    idNumberEditingController.text.trim().isNotEmpty &&
                                    kraEditingController.text.trim().isNotEmpty &&
                                    employerEditingController.text.trim().isNotEmpty &&
                                    emailEditingController.text.trim().isNotEmpty &&
                                    cityEditingController.text.trim().isNotEmpty &&
                                    physicalAddressEditingController.text.trim().isNotEmpty){
                                  makeAPICALL(nameEditingController.text.trim(),
                                      genderEditingController.text.trim(),
                                      nationalityEditingController.text.trim(),
                                      phoneNumberEditingController.text.trim(),
                                      idNumberEditingController.text.trim(),
                                      kraEditingController.text.trim(),
                                      employerEditingController.text.trim(),
                                      emailEditingController.text.trim(),
                                      cityEditingController.text.trim(),
                                      physicalAddressEditingController.text.trim(),
                                      context);
                                }
                              },
                              child:Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 2.0),
                                  child: Text('Next',style: lBodyStyle,)),),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              );
            }),
      ),
    );
  }
}
//RadioButtonPage
class RadioButtonPageAccount extends StatefulWidget {
  const RadioButtonPageAccount({Key? key}) : super(key: key);



  @override
  State<RadioButtonPageAccount> createState() => _RadioButtonPageAccountState();
}

class _RadioButtonPageAccountState extends State<RadioButtonPageAccount> {
  bool _isSelectedNawiri=false;
  bool _isSelectedUngana=false;
  bool _isSelectedBusiness=false;
  bool _isSelectedTabibu=false;
  ganderGroup _value=ganderGroup.Retail;

  bool _isOpen=false;


  @override
  void initState() {
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: ListView(
        children: [
          Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        Radio(
                            value: ganderGroup.Retail,
                            groupValue: _value,
                            onChanged: (ganderGroup?val)=>{
                              setState(() {
                                _value=val!;
                                // makeAPICALLRETAIL();
                              })
                            }),
                        Text('Retail'),
                      ],
                    ),
                    //flex: 1,
                  ),
                  Expanded(child: Row(
                    children: [
                      Radio(
                          value: ganderGroup.Corporate,
                          groupValue: _value,
                          onChanged: (ganderGroup?val){
                            setState(() {
                              _value=val!;
                            });
                          }),
                      Text('Corporate'),
                    ],
                  ),
                    //flex: 1,
                  )
                ],
              ),
              //SizedBox(height: 10,),
              SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  //padding: EdgeInsets.all(2.0),
                  child: (_value==ganderGroup.Retail?Container(
                    child: Column(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.height*0.9,
                          width: MediaQuery.of(context).size.width*0.9,
                          child: Stack(
                            children: [
                              Positioned(
                                  top:45,
                                  // left: 25,
                                  child: Container(
                                    height: MediaQuery.of(context).size.height*0.9,
                                    width: MediaQuery.of(context).size.width*0.9,
                                    child: Column(
                                      children: [
                                        //Text('Hello2')
                                        Expanded(
                                          child: Scaffold(
                                            body: Center(
                                              child: FutureBuilder<List<RetailModel>>(
                                                future: makeAPICALLRETAIL(),
                                                builder: (context, snapshot) {
                                                  if (snapshot.connectionState == ConnectionState.waiting) {
                                                    return CircularProgressIndicator();
                                                  } else {
                                                    final data = snapshot.data;
                                                    return ListView.builder(
                                                        itemCount: data?.length,
                                                        itemBuilder: (context, index) {
                                                          return SingleChildScrollView(
                                                            child: Column(
                                                              children: [
                                                                GestureDetector(
                                                                  onTap: (){
                                                                    setState(() {
                                                                      _isSelectedNawiri=true;
                                                                      _isSelectedUngana=false;
                                                                      _isSelectedTabibu=false;
                                                                      _isSelectedBusiness=false;
                                                                    });
                                                                    _tabController.index=1;
                                                                  },
                                                                  child: SingleChildScrollView(
                                                                    child: Card(
                                                                      color: _isSelectedNawiri ? Colors.yellow:Colors.white,
                                                                      child: ExpansionTile(
                                                                        title:  Text(data![index].categoryName ?? ''),
                                                                        children: [
                                                                          Text(data[index].description ?? ''),
                                                                          Text(data[index].targetUser ?? ''),
                                                                          Divider(height: 20,),
                                                                          Text("No opening balance"),
                                                                          Text("No minimum balance"),
                                                                          Text("No ledger fees"),
                                                                          Text("Free deposits"),
                                                                          Text("Over the counter(OTC) withdrawal fee of Ksh.100.00"),
                                                                          Text("Available in KES,USD,GBP and EURO"),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                                SizedBox(height: 5,),
                                                                GestureDetector(
                                                                  onTap: (){
                                                                    setState(() {
                                                                      _isSelectedNawiri=false;
                                                                      _isSelectedUngana=true;
                                                                      _isSelectedTabibu=false;
                                                                      _isSelectedBusiness=false;
                                                                      _tabController.index=1;
                                                                    });
                                                                  },
                                                                  child: SingleChildScrollView(
                                                                    child: Card(
                                                                      color: _isSelectedUngana ? Colors.yellow:Colors.white,
                                                                      child: ExpansionTile(
                                                                        title:  Text(data[index].categoryName ?? ''),
                                                                        children: [
                                                                          Text(data[index].description ?? ''),
                                                                          Text(data[index].targetUser ?? ''),
                                                                          Text("Suited for Saving group with common agenda"),
                                                                          Text("to acccumulate savings for investments"),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                  ),
                                                                )
                                                              ],
                                                            ),
                                                          );
                                                        });
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      ],
                    ),
                  ):Container(
                    child: Column(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.width*0.9,
                          width: MediaQuery.of(context).size.width*0.9,
                          child: Stack(
                            children: [
                              Positioned(
                                  top:45,
                                  //left: 25,
                                  child: Container(
                                    height: MediaQuery.of(context).size.width*0.9,
                                    width: MediaQuery.of(context).size.width*0.9,
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Scaffold(
                                            body: Center(
                                              child: FutureBuilder<List<RetailModel>>(
                                                future: makeAPICALLRETAILFOR(),
                                                builder: (context, snapshot) {
                                                  if (snapshot.connectionState == ConnectionState.waiting) {
                                                    return CircularProgressIndicator();
                                                  } else {
                                                    final data = snapshot.data;
                                                    //print(data);
                                                    return ListView.builder(
                                                        itemCount: data?.length,
                                                        itemBuilder: (context, index) {
                                                          return Column(
                                                            children: [
                                                              GestureDetector(
                                                                onTap: (){
                                                                  setState(() {
                                                                    _isSelectedNawiri=false;
                                                                    _isSelectedUngana=false;
                                                                    _isSelectedTabibu=false;
                                                                    _isSelectedBusiness=true;
                                                                    _tabController.index = 1;
                                                                  });
                                                                },
                                                                child: SingleChildScrollView(
                                                                  child: Card(
                                                                    color: _isSelectedBusiness ? Colors.yellow:Colors.white,
                                                                    child: ExpansionTile(
                                                                      title:  Text(data![index].categoryName ?? ''),
                                                                      children: [
                                                                        Text(data[index].description ?? ''),
                                                                        Text(data[index].targetUser ?? ''),
                                                                        Text("Openning balance of ksh.2000.00,200 USD"),
                                                                        Text("(FX equivalent of USD.200.00"),
                                                                        Text("No minimum balance"),
                                                                        Text("Ledger fee of KES.300.00(USD.5)"),
                                                                        Text("For the first 15 transactions"),
                                                                        Text("and 20.00 for each extra transactions"),
                                                                        Text("Available in KES,USD,GBP and EURO"),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(height: 5,),
                                                              GestureDetector(
                                                                onTap: (){
                                                                  setState(() {
                                                                    _isSelectedNawiri=false;
                                                                    _isSelectedUngana=false;
                                                                    _isSelectedTabibu=true;
                                                                    _isSelectedBusiness=false;
                                                                    _tabController.index=1;
                                                                  });
                                                                },
                                                                child: SingleChildScrollView(
                                                                  child: Card(
                                                                    color: _isSelectedTabibu ? Colors.yellow:Colors.white,
                                                                    child: ExpansionTile(
                                                                      title:  Text(data[index].categoryName ?? ''),
                                                                      children: [
                                                                        Text(data[index].description ?? ''),
                                                                        Text(data[index].targetUser ?? ''),
                                                                        Text("Sidian Bank in partnership with Mediacal Credit Fund"),
                                                                        Text("East Africa provides tailor-made financila solution to.."),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              )
                                                            ],
                                                          );
                                                        });
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ))
                            ],
                          ),
                        ),
                      ],
                    ),
                  ))
              ),
            ],

          )
        ],
      ),
    );
  }
}


class AccountsPage extends StatefulWidget {
  const AccountsPage({Key? key}) : super(key: key);

  @override
  State<AccountsPage> createState() => _AccountsPageState();
}

class _AccountsPageState extends State<AccountsPage> with SingleTickerProviderStateMixin {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController=TabController(length: 3, vsync: this);
    /*_tabController.addListener(() {
      setState(() {
        globals.tabIndex++;
       //_tabController.index++;
        //DefaultTabController.of(context)!.animateTo(1);
      });
    });*/
    @override
    void dispose() {
      _tabController.dispose();
      // TODO: implement dispose
      super.dispose();
    }
  }
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: _tabController.length,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            actions: [
              IconButton(onPressed: (){}, icon: Icon(Icons.search)),
              IconButton(onPressed: (){}, icon: Icon(Icons.more_vert))
            ],
            bottom: ReadOnlyTabBar(
              child: TabBar(
                tabs: [
                  Tab(text: 'Products',),
                  Tab(text: 'KYC'),
                  Tab(text: "Documents",)
                ],
                controller: _tabController,
              ),
            ),
            title: Text("Accounts"),
          ),
          drawer: MenuDrawer(),
          body: TabBarView(
              physics: const NeverScrollableScrollPhysics(),
              controller: _tabController,
              children: [
                RadioButtonPageAccount(),
                KycTabShowCard(),
                MandateTabShowCard(),
              ]
          ),
        )
    );
  }
}
