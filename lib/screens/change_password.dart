import 'dart:convert';
//import 'dart:js';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidian/globals.dart' as globals;
import 'package:sidian/screens/login.dart';



makeAPICALL(String password,String confirmPassword,BuildContext context)async{
  String resp="";
  String response="";
  String responseDescription="";
  var jsonObj;
  SharedPreferences prefs=await SharedPreferences.getInstance();
  String old_password=prefs.getString("old_password") ?? "";
  String token=prefs.getString("token") ?? "";
  Map map={
    "username": "esb",
    "password": "esb@ao2021",
    "msgType": "1200",
    "processingCode": "999000",
    "serviceCode": "120000",
    "channel": "APP",
    "current_password": old_password.trim(),
    "new_password": password.trim(),
    "token" : token
  };
  resp=(await globals.apiRequest(globals.BASE_URL, map));
  jsonObj=json.decode(resp);
  response=jsonObj['response'];
  responseDescription=jsonObj['responseDescription'];
  Fluttertoast.showToast(
    msg: responseDescription,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
    timeInSecForIosWeb: 1,
    textColor: Colors.white,
    fontSize: 16,
  );
  if(password==confirmPassword){
    if(response=="000"){
      Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage()));
    }
  }
}

/*makeAPICALL(BuildContext context) async{

  if(password==confirmPassword){
    String url=globals.BASE_URL;
    String resp="";
    String responseDescription="";
    String response="";
    String oldPassword="";
        //email="";
    var jsonObj;
    //var accessToken;
    SharedPreferences prefs= await SharedPreferences.getInstance();
     oldPassword=prefs.getString("password") ?? "";
    //String email=prefs.getString("email") ?? "";
    String token=prefs.getString("token") ?? "";

    Map map={
        "username": "esb",
        "password": "esb@ao2021",
        "msgType": "1200",
        "processingCode": "999000",
        "serviceCode": "120000",
        "channel": "APP",
        "current_password": oldPassword,
        "new_password": password.trim(),
        "token" : token
      };
    resp=(await globals.apiRequest(globals.BASE_URL, map));
    jsonObj=json.decode(resp);
    //accessToken=accessToken.fromJson(map);
    response=jsonObj['response'];
    responseDescription=jsonObj['responseDescription'];

    //Navigator.pop(context);
  }
  else {

  }
}*/

// Fluttertoast.showToast(
//       msg: "Password must be a match",
//       toastLength: Toast.LENGTH_LONG,
//       gravity: ToastGravity.BOTTOM,
//       textColor: Colors.white,
//       timeInSecForIosWeb: 1,
//       fontSize: 16,
//     );


class ChangePassword extends StatelessWidget {
   ChangePassword({Key? key}) : super(key: key);

   @override
   showLoaderDialog(BuildContext context){
     AlertDialog alert=AlertDialog(
       content: new Row(
         children: [
           CircularProgressIndicator(),
           Container(margin: EdgeInsets.only(left: 7),child:Text("Loading..." )),
         ],),
     );
     showDialog(barrierDismissible: false,
       context:context,
       builder:(BuildContext context){
         return alert;
       },
     );
   }

  TextEditingController passwordController=TextEditingController();
  TextEditingController confirmPasswordController=TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
          padding: EdgeInsets.all(0),
        child: ListView(
          children: [
            Card(
              color: Colors.lightGreenAccent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)
              ),
              child: Column(
                //align:Alignment.center
                children: [
                  SizedBox(height: 30,),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10.0),
                    child: Text(
                      "Change Password",
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: TextFormField(
                      style: TextStyle(color: Colors.white),
                      obscureText: true,
                      controller: passwordController,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12.0),
                          borderSide: new BorderSide(
                            color: Colors.white70,
                          )
                        ),
                        labelStyle: TextStyle(color: Colors.black),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12.0),
                          borderSide: new BorderSide(
                            color: Colors.white,
                          )
                        ),
                        labelText: 'Password',
                        prefixIcon: const Icon(
                          Icons.password_outlined,
                          color: Colors.white70,
                        )
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: TextFormField(
                      style: TextStyle(color: Colors.white70),
                      obscureText: true,
                      controller: confirmPasswordController,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12.0),
                          borderSide: new BorderSide(
                            color: Colors.white70,
                          ),
                        ),
                        labelStyle: TextStyle(color: Colors.black),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12.0),
                          borderSide: new BorderSide(
                            color: Colors.white,
                          )
                        ),
                        labelText: 'Confirm Password',
                        prefixIcon: const Icon(
                          Icons.password_outlined,
                          color: Colors.white70,
                        )
                      ),
                    ),
                  ),
                  InkWell(
                    enableFeedback: true,
                    child: Container(
                      height: 70,
                      width: 200,
                      padding: EdgeInsets.all(10.0),
                      child: Card(
                        color: Colors.black54,
                        elevation: 5,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(
                            color: Colors.lightGreenAccent,
                          ),
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: TextButton(
                          child: Text(
                            'Change Password',
                            style: TextStyle(
                              color: Colors.white70,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          onPressed: () {
                            showLoaderDialog(context);
                            makeAPICALL(passwordController.text.trim(), confirmPasswordController.text.trim(), context);
                          },
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}



