import 'package:flutter/material.dart';
import 'package:sidian/screens/menu_drawer.dart';
import 'package:sidian/screens/time_pop_screen.dart';

class TimeSheet extends StatelessWidget {
  String? location,title,startTime,endTime,currentDate,agenda;

  TimeSheet({
     required this.location,
     required  this.title,
     required this.startTime,
     required this.endTime,
     required this.currentDate,
     required this.agenda}); //const TimeSheet({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("TimeSheet"),
        actions: [
          IconButton(onPressed: (){}, icon: Icon(Icons.more_vert)),
        ],
        backgroundColor: Colors.black,
      ),
      drawer: MenuDrawer(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: MediaQuery.of(context).size.height*0.9,
              width: MediaQuery.of(context).size.width*0.9,
              child: Stack(
                children: [
                  Positioned(
                      top:45,
                      // left: 25,
                      child: Container(
                        height: MediaQuery.of(context).size.height*0.9,
                        width: MediaQuery.of(context).size.width*0.9,
                        child: Column(
                          children: [
                            Expanded(
                              child: Scaffold(
                                body: Center(
                                  child: ListView.builder(
                                      itemCount: 1,
                                      itemBuilder: (context, index) {
                                        return SingleChildScrollView(
                                          child: Column(
                                            children: [
                                              GestureDetector(
                                                onTap: (){
                                                },
                                                child: SingleChildScrollView(
                                                  child: Card(
                                                    child: ExpansionTile(
                                                      title:  Text('${title}'),
                                                      children: [
                                                        Text('${location}'),
                                                        Text('${startTime}'),
                                                        Divider(height: 20,),
                                                        Text('${endTime}'),
                                                        Text('${currentDate}'),
                                                        Text('${agenda}'),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              //SizedBox(height: 10,),
                                              /*GestureDetector(
                                                onTap: (){
                                                },
                                                child: SingleChildScrollView(
                                                  child: Card(
                                                    child: ExpansionTile(
                                                      title:  Text('${title}'),
                                                      children: [
                                                        Text('${location}'),
                                                        Text('${startTime}'),
                                                        Divider(height: 20,),
                                                        Text('${endTime}'),
                                                        Text('${currentDate}'),
                                                        Text('${agenda}'),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),*/
                                              SizedBox(height: 20,),
                                              Center(
                                                child: Container(
                                                  child: FloatingActionButton(
                                                    onPressed: (){
                                                      Navigator.push(context, MaterialPageRoute(builder: (context)=>TimePopScreen()));
                                                    },
                                                    child: Icon(Icons.add, color: Colors.black, size: 29,),
                                                    backgroundColor: Colors.lightGreen,
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        );
                                      }),
                                ),
                              ),
                            )
                          ],
                        ),
                      )),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
