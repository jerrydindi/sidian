import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:local_auth/local_auth.dart';
import 'package:sidian/model/login_model.dart';
import 'package:sidian/screens/accounts.dart';
import 'package:sidian/screens/change_password.dart';
import 'package:sidian/screens/dashboard.dart';
//import 'package:sidian/screens/dashboardtext.dart';
import 'package:sidian/screens/recovery_page.dart';
//import 'package:sidian/screens/dashboard.dart';
import 'package:sidian/text_style.dart';
import 'package:sidian/globals.dart' as globals;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:device_info_plus/device_info_plus.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String identifier="";
  makeAPICall(String email, String password,BuildContext context) async {
    // print(" === > EMail & Password  --$email -- $pwd ");
    //Function to capture device serial Number
    Future<void> getDeviceSerial() async{
      final DeviceInfoPlugin deviceInfoPlugin=new DeviceInfoPlugin();
      try{
        if (Platform.isAndroid){
          var build = await deviceInfoPlugin.androidInfo;
          setState(() {
            identifier=build.androidId!;
          });
        }else if(Platform.isIOS){
          var data= await deviceInfoPlugin.iosInfo;
          setState(() {
            identifier=data.identifierForVendor!;
          });
        }
      }catch(err){

      }
    }

    String url = globals.BASE_URL;
    String resp = "", responseDescription = "", response = "", token="", changePin="";
    var jsonObj;
    //var jsonDecoder;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map map= {
      "username": "esb",
      "password": "esb@ao2021",
      "msgType": "1200",
      "processingCode": "110000",
      "txnType": "LG",
      "dfaChannel": "app",
      "dfaEmail": email.toLowerCase(),
      "dfaPassword": password.trim(),
      "deviceSerial": identifier
    };

    resp = (await globals.apiRequest(globals.BASE_URL, map));
    print(resp);
    jsonObj = json.decode(resp);
    response = jsonObj['response'];
    responseDescription = jsonObj['responseDescription'];
    token = jsonObj['token'];
    changePin = jsonObj['changePin'];
    Navigator.pop(context);
    Fluttertoast.showToast(
        msg: responseDescription,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        textColor: Colors.white,
        fontSize: 16.0);
    getDeviceSerial();
  //  if(response=="999"){

    //}
    if(changePin=="1") {
      prefs.setString("old_password", password);
      prefs.setString("email", email);
      prefs.setString("token", token);
      Navigator.push(context, MaterialPageRoute(builder: (context) => ChangePassword()));
    }else if (response=="000"){//success
      //prefs.setString("token",token);
      prefs.setString("email", email);
      //String token = jsonObj['token'];
      prefs.setString("token", token);
      prefs.setString("old_password", password);
      //prefs.setString("agentId",);
      String agentId=jsonObj["agent"]["id"];
      prefs.setString("id", agentId);
      prefs.setString("email", email);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => DashBoard()));
    }else
    {}

  }
  final scaffoldKey = GlobalKey<ScaffoldState>();
 // GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
 // late LoginRequestModel requestModel;
  //late LoginResponseModel responseModel;
  bool hidePassword=true;
  //function to capture device id
  //String identifier="";



  //todo=fingurePrint functionality start
  final LocalAuthentication _localAuthentication= LocalAuthentication();

  Future<bool> checkingForBioMetrics() async {
    bool canCheckBiometrics = await _localAuthentication.canCheckBiometrics;
    print(canCheckBiometrics);
    return canCheckBiometrics;
  }

  Future<void> _authenticateMe() async {
// 8. this method opens a dialog for fingerprint authentication.
//    we do not need to create a dialog nut it popsup from device natively.
    bool authenticated = false;
    try {
      authenticated = await _localAuthentication.authenticateWithBiometrics(
        localizedReason: "Authenticate for Testing", // message for dialog
        useErrorDialogs: true, // show error in dialog
        stickyAuth: true, // native process
      );
      setState(() {
       // _message = authenticated ? "Authorized" : "Not Authorized";
      });
    } catch (e) {
      print(e);
    }
    if (!mounted) return;
  }
  @override
  void initState() {
// TODO: implement initState
    checkingForBioMetrics();
    super.initState();
  }

 // Future<void> _authenticateMe () async {
   // bool _authenticated=false;
    //try{
     // authenticated=await _localAuthentication
   // }
 // }

  @override
  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Loading...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  TextEditingController _emailController=TextEditingController();
  TextEditingController _passwordController=TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/dfa.png'),
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(
                    Colors.lightGreenAccent.withOpacity(0),
                    BlendMode. hardLight,
                  )
              )
          ),
        ),
        Scaffold(
          key: scaffoldKey,
          backgroundColor: Colors.transparent,
          body: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 40),
                  child: Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                              padding: const EdgeInsets.symmetric(vertical: 5),
                            child: Container(
                              child: TextFormField(
                                style: yBodyStyle,
                                keyboardType: TextInputType.emailAddress,
                                textInputAction: TextInputAction.next,
                               controller: _emailController,
                               // onSaved: (input)=>requestModel.email=input!,
                                //validator: (input) => !input!.contains('@')? "Email Id should be valid" : null,
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.symmetric(vertical: 16),
                                  border: InputBorder.none,
                                  hintText: "Email",
                                  hintStyle: wBodyStyle,
                                  prefixIcon: Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 5),
                                    child: Icon(
                                      FontAwesomeIcons.solidEnvelope,
                                      color: Colors.black,
                                      size: 20,
                                    ),
                                  )
                                ),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white10.withOpacity(0.5),borderRadius: BorderRadius.circular(10)
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5),
                            child: Container(
                              child: TextFormField(
                                style: yBodyStyle,
                                obscureText: hidePassword,
                                //keyboardType: TextInputType.visiblePassword,
                                textInputAction: TextInputAction.done,
                                controller: _passwordController,
                                //onSaved: (input)=>requestModel.password=input!,
                               // validator: (input)=>input!.length < 3?'Password should be more than 3 character':null,
                                decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.symmetric(vertical: 16),
                                    border: InputBorder.none,
                                    hintText: "Password",
                                    hintStyle: wBodyStyle,
                                    prefixIcon: Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 5),
                                      child: Icon(
                                        FontAwesomeIcons.lock,
                                        color: Colors.black,
                                        size: 20,
                                      ),
                                    ),
                                  suffixIcon: IconButton(onPressed: (){
                                    setState(() {
                                      hidePassword=!hidePassword;
                                    });
                                  },
                                      icon: Icon(hidePassword?Icons.visibility_off:Icons.visibility,color: Colors.black,))
                                ),
                              ),
                              decoration: BoxDecoration(
                                  color: Colors.white10.withOpacity(0.5),borderRadius: BorderRadius.circular(10)
                              ),
                            ),
                          ),
                          Column(
                            children: [
                              SizedBox(height: 20,),
                              Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  color: Colors.yellow.shade300,
                                  borderRadius: BorderRadius.circular(2)
                                ),
                                child: TextButton(
                                    onPressed: (){
                                      showLoaderDialog(context);
                                      makeAPICall(
                                          _emailController.text.trim(),
                                          _passwordController.text.trim(),
                                          context);
                                    },
                                    child: Padding(
                                        padding: const EdgeInsets.symmetric(vertical: 0.0),
                                      child: Text('Login',style: lBodyStyle,),
                                    )
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  FlatButton(
                                      onPressed: (){
                                        Navigator.push(context, MaterialPageRoute(builder: (context)=>RecoveryPage()));
                                      },
                                      child: Text('Forget password',style: fBodyStyle,)),
                                  Container(
                                    child: IconButton(
                                        onPressed: (){_authenticateMe();},
                                        icon: Icon(Icons.fingerprint_rounded,color: Colors.white,size: 50,)),
                                  )
                                ],
                              )
                            ],
                          ),
                          SizedBox(height: 2,),
                          Container(
                            child: FlatButton(
                                onPressed: (){
                                  Navigator.pushReplacement(context,
                                      MaterialPageRoute(builder: (context)=>DashBoard()));
                                },
                                child: Text("Register",style: fBodyStyle,)
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}
