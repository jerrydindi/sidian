import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:async';
import 'package:image_picker/image_picker.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidian/globals.dart' as globals;
import 'package:sidian/screens/dashboard.dart';
import 'package:sidian/screens/recovery_page.dart';
import 'dart:convert';

import '../text_style.dart';
import 'dart:developer';
import '../model/retail_loan_model.dart';
import 'package:sidian/model/retail_model.dart';
import 'menu_drawer.dart';

//Api call For mandatePage
makeAPICALLMANDATE(String kraCertificate,BuildContext context) async{
  String url=globals.BASE_URL;
  String resp="";
  String response="";
  String responseDescription="";
  var jsonObj;

  SharedPreferences prefs=await SharedPreferences.getInstance();
  String token=prefs.getString("token") ?? "";
  String agentId=prefs.getString("id") ?? "";
  String name=prefs.getString("name") ?? "";
  String gender=prefs.getString("gender") ?? "";
  String nationality=prefs.getString("nationality") ?? "";
  String phoneNumber=prefs.getString("phone_number") ?? "";
  String idNumber=prefs.getString("id_number") ?? "";
  String kra=prefs.getString("kra") ?? "";
  String occupation=prefs.getString("occupation") ?? "";
  String employer=prefs.getString("employer") ?? "";
  String email=prefs.getString("email") ?? "";
  String city=prefs.getString("city") ?? "";
  String type=prefs.getString("type") ?? "";
  String amount=prefs.getString("amount") ?? "";
  String startTime=prefs.getString("start_time") ?? "";
  String maturityDate=prefs.getString("maturity_date") ?? "";

  Map map={
    "username": "esb",
    "password": "esb@ao2021",
    "msgType": "1200",
    "processingCode": "999000",
    "serviceCode": "250000",
    "name": name,
    "gender": gender,
    "nationality": nationality,
    "phoneNumber": phoneNumber,
    "dob": "27-11-1995",
    "idNumber": idNumber,
    "tinNo": kra,
    "occupation": occupation,
    "employer": employer,
    "email": email,
    "city": city,
    "agentId": agentId,
    "type": type,
    "duration":"12",
    "interest":"5",
    "amount":amount,
    "startDate":startTime,
    "maturityDate":maturityDate,
    "tinNoImage":kraCertificate,
    "token" : token
  };

  resp=(await globals.apiRequest(globals.BASE_URL, map));
  print(resp);
  jsonObj=json.decode(resp);
  response=jsonObj["response"];
  responseDescription=jsonObj["responseDescription"];

  Fluttertoast.showToast(
      msg: responseDescription,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      textColor: Colors.white,
      fontSize: 16.0);
  if(response=="999"){

  }else if(response=="000"){
    prefs.setString("kraCertificate", kraCertificate);
    _tabController.index=0;
   //Navigator.push(context, MaterialPageRoute(builder: (context)=>DashBoard()));
    //_tabController.index;
  }
}

//APICall for kyc
makeAPICALL(String name,String gender,String nationality,String idNumber,String kra,
    String occupation,String employer,String email,String phoneNumber,String city,String type,
    String amount,String startTime,String maturityDate,BuildContext context) async{
  /*String url=globals.BASE_URL;
  String resp='';
  String response='';
  String responseDescription='';
  var jsonObj;*/

  SharedPreferences prefs=await SharedPreferences.getInstance();
  //String token=prefs.getString("token") ?? '';
  //String agentId=prefs.getString("id") ?? "";
  /*Map map={
    "username": "esb",
    "password": "esb@ao2021",
    "msgType": "1200",
    "processingCode": "999000",
    "serviceCode": "250000",
    "name": name,
    "gender": gender,
    "nationality": nationality,
    "phoneNumber": phoneNumber,
    "dob": "27-11-1995",
    "idNumber": idNumber,
    "tinNo": kra,
    "occupation": occupation,
    "employer": employer,
    "email": email,
    "city": city,
    "agentId": agentId,
    "type": type,
    "duration":"12",
    "interest":"5",
    "amount":amount,
    "startDate":startTime,
    "maturityDate":maturityDate,
    "tinNoImage":"dsjhbvsjhdadsjhvjhwaewgvytewvavonanvalawjevawyjfbweahfiwuefnOUFGEYAWBVJR",
    "token" : token
  };*/
  /*resp=(await globals.apiRequest(globals.BASE_URL, map));
  print(resp);
  jsonObj=json.decode(resp);
  response=jsonObj["response"];
  responseDescription=jsonObj["responseDescription"];*/
  //Navigator.pop(context);
  /*Fluttertoast.showToast(
      msg: responseDescription,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      textColor: Colors.white,
      fontSize: 16.0);*/
  prefs.setString("name", name);
  prefs.setString("gender", gender);
  prefs.setString("nationality", nationality);
  prefs.setString("phone_number", phoneNumber);
  prefs.setString("id-number", idNumber);
  prefs.setString("kra", kra);
  prefs.setString("occupation", occupation);
  prefs.setString("employer", employer);
  prefs.setString("email", email);
  prefs.setString("city", city);
  prefs.setString("type", type);
  prefs.setString("amount", amount);
  prefs.setString("start_time", startTime);
  prefs.setString("maturity_date", maturityDate);
  _tabController.index=2;
  //Navigator.push(context, MaterialPageRoute(builder: (context) => MandateLoan()));
  //_tabController.index=2;
  /*if(response=="999"){

  }else if(response=="000"){
    /*prefs.setString("name", name);
    prefs.setString("gender", gender);
    prefs.setString("nationality", nationality);
    prefs.setString("phone_number", phoneNumber);
    prefs.setString("id-number", idNumber);
    prefs.setString("kra", kra);
    prefs.setString("occupation", occupation);
    prefs.setString("employer", employer);
    prefs.setString("email", email);
    prefs.setString("city", city);
    prefs.setString("type", type);
    prefs.setString("amount", amount);
    prefs.setString("start_time", startTime);
    prefs.setString("maturity_date", maturityDate);

    //Navigator.push(context, MaterialPageRoute(builder: (context) => MandateLoan()));
    _tabController.index=2;*/
  }*/

}

//api call and function for radio button
Future<List<RetailLoanModel>> makeAPICALLRETAIL() async{
  // String url=globals.BASE_URL;
  String resp="";
  String response="";
  String responseDescription="";
  var jsonObj;

  SharedPreferences prefs=await SharedPreferences.getInstance();
  String token=prefs.getString("token") ?? "";
  Map map={
    "username": "esb",
    "password": "esb@ao2021",
    "msgType": "1200",
    "processingCode": "999000",
    "serviceCode": "500000",
    "channel": "APP",
    "token": token
  };

  resp=(await globals.apiRequest(globals.BASE_URL, map));
  jsonObj=json.decode(resp);
  List<RetailLoanModel> retailData=[];
  response=jsonObj["response"];
  responseDescription=jsonObj["responseDescription"];
  var dataList=jsonObj["data"];
  Fluttertoast.showToast(
      msg: responseDescription,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      textColor: Colors.white,
      fontSize: 16.0);
  if(response=="999"){}
  else if(response=="000"){
    prefs.setString("data__retail", response);
    for(var item in dataList){
      retailData.add(RetailLoanModel.fromJson(item));
    }
    log(retailData.toString());
  }
  return retailData;
}

//corporate choice
Future<List<RetailModel>> makeAPICALLCORPORATE() async{
  String resp="";
  String response="";
  String responseDescription="";
  var jsonObj;
  SharedPreferences prefs=await SharedPreferences.getInstance();
  String token=prefs.getString("token") ?? "";
  Map map={
    "username": "esb",
    "password": "esb@ao2021",
    "msgType": "1200",
    "processingCode": "999000",
    "serviceCode": "501000",
    "channel": "APP",
    "token" : token
  };
  resp=(await globals.apiRequest(globals.BASE_URL, map));
  print(resp);
  jsonObj=json.decode(resp);
  List<RetailModel> retailData=[];
  response=jsonObj["response"];
  responseDescription=jsonObj["responseDescription"];
  var dataList=jsonObj["data"];
  Fluttertoast.showToast(
      msg: responseDescription,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      textColor: Colors.white,
      fontSize: 16.0);
  if(response=="999"){}
  else if(response=="000"){
    prefs.setString("data__retail", response);
    for(var item in dataList) {
      retailData.add(RetailModel.fromJson(item));
    }
    log(retailData.toString());
  }
  return retailData;
}
enum ganderGroup{Retail,Corporate}
late final TabController _tabController;

//Mandate Tab page
class MandateLoan extends StatefulWidget {
  const MandateLoan({Key? key}) : super(key: key);

  @override
  State<MandateLoan> createState() => _MandateLoanState();
}

class _MandateLoanState extends State<MandateLoan> {

  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Submitting...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  TextEditingController kraCertificateEditingController=TextEditingController();
  bool _validate=false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView.builder(
            itemCount: 1,
            itemBuilder: (context,index){
              return Container(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('KRA Certificate',style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                        SizedBox(width: 20,),
                        IconButton(onPressed: (){
                          //getImage(ImageSource.camera);
                        },
                          icon: Icon(Icons.document_scanner,color: Colors.black,),)
                      ],
                    ),
                    Container(
                      child: TextField(
                        //enabled: false,
                        controller: kraCertificateEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Kra Certificate",
                          labelText: "Kra Certificate",
                          border: OutlineInputBorder(),
                          errorText: _validate ? "PLease fill in the field its required":null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.lightGreen,
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: FlatButton(
                              onPressed:(){
                                //Navigator.pop(context);
                                _tabController.index=1;
                              },
                              child:Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 2.0),
                                  child: Text('Previous',style: lBodyStyle,)),),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.lightGreen,
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: FlatButton(
                              onPressed:(){
                                setState(() {
                                  kraCertificateEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                });
                                if(kraCertificateEditingController.text.trim().isNotEmpty){
                                  makeAPICALLMANDATE(kraCertificateEditingController.text.trim(), context);
                                }
                                /*showLoaderDialog(context);
                                if(kraCertificateEditingController==null){
                                  Text("Please fill in the KRA Certificate pin");
                                }else{
                                  makeAPICALLMANDATE(kraCertificateEditingController.text.trim(), context);
                                }*/
                                // Navigator.push(context, MaterialPageRoute(builder: (context)=>DashBoard()));
                              },
                              child:Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 2.0),
                                  child: Text('Submit',style: lBodyStyle,)),),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              );
            }),
      ),
    );
  }
}

//Class to denie scrolling of Tabcontroller Manually
class ReadOnlyTabBar extends StatelessWidget implements PreferredSizeWidget {
  final TabBar child;

  const ReadOnlyTabBar({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(child: child);
  }

  @override
  Size get preferredSize => child.preferredSize;
}
//Kyctab page
class KycTabForLoan extends StatefulWidget {
  const KycTabForLoan({Key? key}) : super(key: key);

  @override
  State<KycTabForLoan> createState() => _KycTabForLoanState();
}

class _KycTabForLoanState extends State<KycTabForLoan> {
  showLoaderDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Saving...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  showForEmptyTextDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(),
          Container(
              margin: EdgeInsets.only(left: 7), child: Text("Fill the black...")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  TimeOfDay startTime=TimeOfDay.now();
  DateTime currentDate=DateTime.now();
  DateTime maturityDate=DateTime.now();
  bool _isMaturitySelected=false;
  bool _isTimeSelected=false;
  bool _isDateSelected=false;
  bool _validate=false;
  TextEditingController nameEditingController=TextEditingController();
  TextEditingController genderEditingController=TextEditingController();
  TextEditingController nationalityEditingController=TextEditingController();
  //TextEditingController _EditingController=TextEditingController(); itakuwa listtile
  TextEditingController idNumberEditingController=TextEditingController();
  TextEditingController cityEditingController=TextEditingController();
  TextEditingController kraEditingController=TextEditingController();
  TextEditingController occupationEditingController=TextEditingController();
  TextEditingController employerEditingController=TextEditingController();
  TextEditingController typeEditingController=TextEditingController();
  TextEditingController durationEditingController=TextEditingController();
  //TextEditingController _interestEditingController=TextEditingController();
  TextEditingController amountEditingController=TextEditingController();
  //TextEditingController _startEditingController=TextEditingController(); Listtile
  //maturity date listtile


  TextEditingController phoneNumberEditingController=TextEditingController();
  TextEditingController emailEditingController=TextEditingController();


  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? pickedTime=await showTimePicker(
      context: context,
      initialTime: startTime,
    );

    if(pickedTime!=null && pickedTime!=startTime){
      setState(() {
        startTime=pickedTime;
        _isTimeSelected=true;
      });
    }
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? pickedDate=await showDatePicker(
        context: context,
        initialDate: currentDate,
        firstDate: DateTime(2022),
        lastDate: DateTime(2040),
        useRootNavigator: true
    );

    if(pickedDate != null && pickedDate != currentDate){
      setState(() {
        currentDate=pickedDate;
        _isMaturitySelected=true;
      });
    }
  }
  //for image scan
  bool textScanning=false;
  XFile? imageFile;

  String scannedText="";

  void getImage(ImageSource source) async {
    try{
      final pickedImage=await ImagePicker().pickImage(source: source);
      if(pickedImage != null){
        textScanning=true;
        imageFile=pickedImage;
        setState(() {
          getRecognisedText(pickedImage);
        });
      }
    }catch(e){
      textScanning=false;
      imageFile=null;
      scannedText="Error occured while scanning the text";
      setState(() {
      });
    }
  }
  //function for recorgnize text
  void getRecognisedText(XFile image) async {
    final inputImage = InputImage.fromFilePath(image.path);
    final textDetector = GoogleMlKit.vision.textDetector();
    RecognisedText recognisedText = await textDetector.processImage(inputImage);
    await textDetector.close();
    scannedText = "";
    var strArr;
    for (TextBlock block in recognisedText.blocks) {
      for (TextLine line in block.lines) {
        scannedText = scannedText + line.text + "\n";
        strArr=scannedText;
      }
    }
    textScanning = false;

    setState(() {
      nameEditingController.text=strArr[3];
      genderEditingController.text=strArr[4];
      nationalityEditingController.text=strArr[1];
      idNumberEditingController.text=strArr[2];
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        child: ListView.builder(
            itemCount: 1,
            itemBuilder: (context, index) {
              return Container(
                padding: EdgeInsets.only(left: 5.0,right: 5.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text('Income Details',style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                        SizedBox(width: 20,),
                        IconButton(onPressed: (){
                          getImage(ImageSource.camera);
                        },
                          icon: Icon(Icons.document_scanner,color: Colors.black,),)
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: nameEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Name",
                          labelText: "Name",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: genderEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Gender",
                          labelText: "Gender",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: nationalityEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Nationality",
                          labelText: "Nationality",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: idNumberEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Id Number",
                          labelText: "Id Number",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: cityEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "City",
                          labelText: "City",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: kraEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Kra pin",
                          labelText: "Kra pin",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: occupationEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Occupation",
                          labelText: "Occupation",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: employerEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Employer",
                          labelText: "Employer",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: typeEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Type",
                          labelText: "Type",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: durationEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Duration",
                          labelText: "Duration",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: amountEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Amount",
                          labelText: "Amount",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: phoneNumberEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Phone Number",
                          labelText: "Phone Number",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: TextField(
                        //enabled: false,
                        controller: emailEditingController,
                        //scannedText;
                        decoration: InputDecoration(
                          hintText: "Email",
                          labelText: "Email",
                          border: OutlineInputBorder(),
                          errorText: _validate ? 'Please fill in the field':null,
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(26)
                      ),
                    ),
                    SizedBox(height:5,),
                    Card(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: ListTile(
                        title: _isTimeSelected ? Text('${startTime.format(context).toString()}'):Text("Start time"),
                        trailing: Icon(Icons.edit),
                        onTap: (){
                          _selectTime(context);
                        },
                      ),
                    ),
                    SizedBox(height:5,),
                    Card(
                      margin: const EdgeInsets.only(left: 10,right: 10),
                      child: ListTile(
                        title:_isMaturitySelected ? Text('${maturityDate.toString()}'):Text('Maturity Date'),
                        trailing: Icon(Icons.edit),
                        onTap: (){
                          _selectDate(context);
                        },
                      ),
                    ),
                    SizedBox(height:5,),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.lightGreen,
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: FlatButton(
                              onPressed:(){
                                //Navigator.pop(context);
                                _tabController.index=0;
                              },
                              child:Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 0.0),
                                  child: Text('Previous',style: lBodyStyle,)),),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.lightGreen,
                                borderRadius: BorderRadius.circular(8)
                            ),
                            child: FlatButton(
                              onPressed:() {
                                setState(() {
                                  nameEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  genderEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  nationalityEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  idNumberEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  kraEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  occupationEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  employerEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  emailEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  phoneNumberEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  cityEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  typeEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                  amountEditingController.text.trim().isEmpty ? _validate=true:_validate=false;
                                });
                                if(nameEditingController.text.trim().isNotEmpty &&
                                    genderEditingController.text.trim().isNotEmpty &&
                                    nationalityEditingController.text.trim().isNotEmpty &&
                                    idNumberEditingController.text.trim().isNotEmpty &&
                                    kraEditingController.text.trim().isNotEmpty &&
                                    occupationEditingController.text.trim().isNotEmpty &&
                                    employerEditingController.text.trim().isNotEmpty &&
                                    emailEditingController.text.trim().isNotEmpty &&
                                    phoneNumberEditingController.text.trim().isNotEmpty &&
                                    typeEditingController.text.trim().isNotEmpty &&
                                    amountEditingController.text.trim().isNotEmpty){
                                  makeAPICALL(nameEditingController.text.trim(),
                                      genderEditingController.text.trim(),
                                      nationalityEditingController.text.trim(),
                                      idNumberEditingController.text.trim(),
                                      kraEditingController.text.trim(),
                                      occupationEditingController.text.trim(),
                                      employerEditingController.text.trim(),
                                      emailEditingController.text.trim(),
                                      phoneNumberEditingController.text.trim(),
                                      cityEditingController.text.trim(),
                                      typeEditingController.text.trim(),
                                      amountEditingController.text.trim(),
                                      startTime.format(context).toString(),
                                      maturityDate.toString(),
                                      context);
                                }
                              },
                              child:Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 2.0),
                                  child: Text('Next',style: lBodyStyle,)),),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )/*Container(
                child: Column(
                  children: [
                    Container(
                      height: 380,
                      child: Stack(
                        children: [
                          Positioned(
                              top: 50,
                             // left: 20,
                              child: Material(
                                child: Container(
                                  height: 350.0,
                                  width: MediaQuery.of(context).size.width*0.9,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(8.0),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.3),
                                          offset: Offset(-10.0, 10),
                                          blurRadius: 20.0,
                                          spreadRadius: 4.0,
                                        )
                                      ]
                                  ),
                                ),
                              )
                          ),
                          Positioned(
                              top: 55,
                              //left: 25,
                              child: Expanded(
                                  child: SingleChildScrollView(
                                    child: Container(
                                      height: 900,
                                      width: 250,
                                      child: Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Text('Income Details',style: TextStyle(fontSize: 20,color: Colors.black,fontWeight: FontWeight.bold),),
                                              SizedBox(width: 20,),
                                              IconButton(onPressed: (){
                                                getImage(ImageSource.camera);
                                              },
                                                icon: Icon(Icons.document_scanner,color: Colors.black,),)
                                            ],
                                          ),
                                          Container(
                                            child: TextFormField(
                                              // enabled: false,
                                              controller: nameEditingController,
                                              //scannedText;
                                              decoration: InputDecoration(
                                                hintText: "Name",
                                                labelText: "Name",
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                          SizedBox(height:5,),
                                          Container(
                                            child: TextFormField(
                                              controller: genderEditingController,
                                              decoration: InputDecoration(
                                                hintText: "Gender",
                                                labelText: "Gender",
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                          SizedBox(height: 5,),
                                          //Text(scannedText),
                                          Container(
                                            child: TextFormField(
                                              controller: nationalityEditingController,
                                              decoration: InputDecoration(
                                                hintText: "nationality",
                                                labelText: 'nationality',
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                          SizedBox(height: 5,),
                                          Container(
                                            child: TextField(
                                              controller: idNumberEditingController,
                                              decoration: InputDecoration(
                                                hintText: "Id Number",
                                                labelText: "Id Number",
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                          SizedBox(height: 5,),
                                          Container(
                                            child: TextFormField(
                                              controller: kraEditingController,
                                              decoration: InputDecoration(
                                                hintText: "kra pin",
                                                labelText: "kra pin",
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                          SizedBox(height:5,),
                                          Container(
                                            child: TextFormField(
                                              controller: occupationEditingController,
                                              decoration: InputDecoration(
                                                hintText: "occupation",
                                                labelText: "occupation",
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                          SizedBox(height: 5,),
                                          Container(
                                            child: TextFormField(
                                              controller: employerEditingController,
                                              decoration: InputDecoration(
                                                hintText: "Employer",
                                                labelText: "Employer",
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                          SizedBox(height: 5,),
                                          Container(
                                            child: TextFormField(
                                              controller: typeEditingController,
                                              decoration: InputDecoration(
                                                hintText: "Type",
                                                labelText: "Type",
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                          SizedBox(height: 5,),
                                          Container(
                                            child: TextFormField(
                                              controller: durationEditingController,
                                              decoration: InputDecoration(
                                                hintText: "Duration",
                                                labelText: "Duration",
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                          SizedBox(height: 5,),
                                          Container(
                                            child: TextFormField(
                                              controller: amountEditingController,
                                              decoration: InputDecoration(
                                                hintText: "Amount",
                                                labelText: "Amount",
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                          SizedBox(height: 5,),
                                          Container(
                                            child: TextFormField(
                                              controller: cityEditingController,
                                              decoration: InputDecoration(
                                                hintText: "Amount",
                                                labelText: "Amount",
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                          SizedBox(height: 5,),
                                          Card(
                                            child: ListTile(
                                              title: _isTimeSelected ? Text('${startTime.format(context).toString()}'):Text("Start time"),
                                              trailing: Icon(Icons.edit),
                                              onTap: (){
                                                _selectTime(context);
                                              },
                                            ),
                                          ),
                                        //  SizedBox(height: 5,),
                                          /*Card(
                                            child: ListTile(
                                              title:_isDateSelected ? Text('${currentDate.toString()}'):Text('Date'),
                                              trailing: Icon(Icons.edit),
                                              onTap: (){
                                                _selectDate(context);
                                              },
                                            ),
                                          ),*/
                                          SizedBox(height: 5,),
                                          Card(
                                            child: ListTile(
                                              title:_isMaturitySelected ? Text('${maturityDate.toString()}'):Text('Maturity Date'),
                                              trailing: Icon(Icons.edit),
                                              onTap: (){
                                                _selectDate(context);
                                              },
                                            ),
                                          ),
                                          SizedBox(height: 10,),
                                          Container(
                                            child: TextFormField(
                                              controller: phoneNumberEditingController,
                                              decoration: InputDecoration(
                                                hintText: "Phone Number",
                                                labelText: "Phone Number",
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                          SizedBox(height:5,),
                                          Container(
                                            child: TextFormField(
                                              controller: emailEditingController,
                                              decoration: InputDecoration(
                                                hintText: "Email Address",
                                                labelText: "Email Address",
                                                border: OutlineInputBorder(),
                                              ),
                                            ),
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(26)
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ))
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.lightGreen,
                                borderRadius: BorderRadius.circular(16)
                            ),
                            child: FlatButton(
                              onPressed:(){
                                Navigator.pop(context);
                              },
                              child:Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 6.0),
                                  child: Text('Previous',style: lBodyStyle,)),),
                          ),
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.lightGreen,
                                borderRadius: BorderRadius.circular(16)
                            ),
                            child: FlatButton(
                              onPressed:(){
                                showLoaderDialog(context);
                                makeAPICALL(nameEditingController.text.trim(),
                                    genderEditingController.text.trim(),
                                    nationalityEditingController.text.trim(),
                                    idNumberEditingController.text.trim(),
                                    kraEditingController.text.trim(),
                                    occupationEditingController.text.trim(),
                                    employerEditingController.text.trim(),
                                    emailEditingController.text.trim(),
                                    phoneNumberEditingController.text.trim(),
                                    cityEditingController.text.trim(),
                                    typeEditingController.text.trim(),
                                    amountEditingController.text.trim(),
                                    startTime.format(context).toString(),
                                    maturityDate.toString(), context);
                                //Navigator.push(context, MaterialPageRoute(builder: (context)=>MandateLoan()));
                              },
                              child:Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 6.0),
                                  child: Text('Next',style: lBodyStyle,)),),
                          )
                        ],
                      ),
                    )
                    //Text(scannedText),
                  ],
                ),
              )*/;
            }),
      ),
    );
  }
}
//RadioButton page
class RadioButtonPageLoan extends StatefulWidget {
  const RadioButtonPageLoan({Key? key}) : super(key: key);

  @override
  State<RadioButtonPageLoan> createState() => _RadioButtonPageLoanState();
}

class _RadioButtonPageLoanState extends State<RadioButtonPageLoan> {
  bool _isSelectedNawiri=false;
  bool _isSelectedUngana=false;
  bool _isSelectedBusiness=false;
  bool _isSelectedTabibu=false;
  ganderGroup _value=ganderGroup.Retail;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: ListView(
        children: [
          Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        Radio(
                            value: ganderGroup.Retail,
                            groupValue: _value,
                            onChanged: (ganderGroup?val){
                              setState(() {
                                _value=val!;
                                //Navigator.push(context, MaterialPageRoute(builder: (context)=>RetailSelectionCard()));
                              });
                            }),
                        Text('Retail')
                      ],
                    ),
                    //flex: 1,
                  ),
                  Expanded(child: Row(
                    children: [
                      Radio(
                          value: ganderGroup.Corporate,
                          groupValue: _value,
                          onChanged: (ganderGroup?val){
                            setState(() {
                              _value=val!;
                              //Navigator.push(context, MaterialPageRoute(builder: (context)=>RetailSelectionCard()));
                            });
                          }),
                      Text('Corporate'),
                    ],
                  ),
                    //flex: 1,
                  )
                ],
              ),
              //SizedBox(height: 10,),
              SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  //padding: EdgeInsets.all(2.0),
                  child: (_value==ganderGroup.Retail?Container(
                    child: Column(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.width*0.9,
                          width: MediaQuery.of(context).size.width*0.9,
                          child: Stack(
                            children: [
                              Positioned(
                                  top:45,
                                 // left: 25,
                                  child: Container(
                                    height: MediaQuery.of(context).size.width*0.9,
                                    width: MediaQuery.of(context).size.width*0.9,
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Scaffold(
                                            body: Center(
                                              child: FutureBuilder<List<RetailLoanModel>>(
                                                future: makeAPICALLRETAIL(),
                                                builder: (context, snapshot) {
                                                  if (snapshot.connectionState == ConnectionState.waiting) {
                                                    return CircularProgressIndicator();
                                                  } else {
                                                    final data = snapshot.data;
                                                    return ListView.builder(
                                                        itemCount: data?.length,
                                                        itemBuilder: (context, index) {
                                                          return Column(
                                                            children: [
                                                              GestureDetector(
                                                                onTap: (){
                                                                  setState(() {
                                                                    _isSelectedNawiri=true;
                                                                    _isSelectedUngana=false;
                                                                    _isSelectedTabibu=false;
                                                                    _isSelectedBusiness=false;
                                                                    _tabController.index=1;
                                                                  });
                                                                },
                                                                child: SingleChildScrollView(
                                                                  child: Card(
                                                                    color: _isSelectedNawiri ? Colors.yellow:Colors.white,
                                                                    child: ExpansionTile(
                                                                      title:  Text(data![index].categoryName ?? ''),
                                                                      children: [
                                                                        Text(data[index].description ?? ''),
                                                                        Text(data[index].targetUser ?? ''),
                                                                        Divider(height: 20,),
                                                                        Text("Openning balance of ksh.2000.00,200 USD"),
                                                                        Text("(FX equivalent of USD.200.00"),
                                                                        Text("No minimum balance"),
                                                                        Text("Ledger fee of KES.300.00(USD.5)"),
                                                                        Text("For the first 15 transactions"),
                                                                        Text("and 20.00 for each extra transactions"),
                                                                        Text("Available in KES,USD,GBP and EURO"),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(height: 5,),
                                                              GestureDetector(
                                                                onTap: (){
                                                                  setState(() {
                                                                    _isSelectedNawiri=false;
                                                                    _isSelectedUngana=true;
                                                                    _isSelectedTabibu=false;
                                                                    _isSelectedBusiness=false;
                                                                    _tabController.index=1;
                                                                  });
                                                                },
                                                                child: SingleChildScrollView(
                                                                  child: Card(
                                                                    color: _isSelectedUngana ? Colors.yellow:Colors.white,
                                                                    child: ExpansionTile(
                                                                      title:  Text(data[index].categoryName ?? ''),
                                                                      children: [
                                                                        Text(data[index].description ?? ''),
                                                                        Text(data[index].targetUser ?? ''),
                                                                        Text("Sidian Bank in partnership with Mediacal Credit Fund"),
                                                                        Text("East Africa provides tailor-made financila solution to.."),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          );
                                                        });
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ))
                            ],
                          ),
                        ),
                        //SizedBox(height: 15,),
                        /*Container(
                          height: 250,
                          width: MediaQuery.of(context).size.width*0.9,
                          child: Stack(
                            children: [
                              Positioned(
                                  top:45,
                                  left: 25,
                                  child: Container(
                                    height: 600,
                                    width: 250,
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Scaffold(
                                            body: Center(
                                              child: FutureBuilder<List<RetailLoanModel>>(
                                                future: makeAPICALLRETAIL(),
                                                builder: (context, snapshot) {
                                                  if (snapshot.connectionState == ConnectionState.waiting) {
                                                    return CircularProgressIndicator();
                                                  } else {
                                                    final data = snapshot.data;
                                                    return ListView.builder(
                                                        itemCount: data?.length,
                                                        itemBuilder: (context, index) {
                                                          return GestureDetector(
                                                            onTap: (){
                                                              setState(() {
                                                                _isSelectedUngana=true;
                                                                _isSelectedNawiri=false;
                                                                _isSelectedBusiness=false;
                                                                _isSelectedTabibu=false;
                                                                DefaultTabController.of(context)!.animateTo(2);
                                                              });
                                                            },
                                                            child: Card(
                                                              color: _isSelectedUngana ? Colors.yellow:Colors.white,
                                                              child: Column(
                                                                children: [
                                                                  Text(data![index].id ?? ''),
                                                                  Text(data[index].categoryName ?? ''),
                                                                  Text(data[index].description ?? ''),
                                                                  Text(data[index].targetUser ?? ''),
                                                                ],
                                                              ),
                                                            ),
                                                          );
                                                        });
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ))
                            ],
                          ),
                        ),*/
                      ],
                    ),
                  ):Container(
                    child: Column(
                      children: [
                        Container(
                          height: MediaQuery.of(context).size.width*0.9,
                          width: MediaQuery.of(context).size.width*0.9,
                          child: Stack(
                            children: [
                              Positioned(
                                  top:45,
                                  //left: 25,
                                  child: Container(
                                    height: MediaQuery.of(context).size.width*0.9,
                                    width: MediaQuery.of(context).size.width*0.9,
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Scaffold(
                                            body: Center(
                                              child: FutureBuilder<List<RetailModel>>(
                                                future: makeAPICALLCORPORATE(),
                                                builder: (context, snapshot) {
                                                  if (snapshot.connectionState == ConnectionState.waiting) {
                                                    return CircularProgressIndicator();
                                                  } else {
                                                    final data = snapshot.data;
                                                    //print(data);
                                                    return ListView.builder(
                                                        itemCount: data?.length,
                                                        itemBuilder: (context, index) {
                                                          return Column(
                                                            children: [
                                                              GestureDetector(
                                                                onTap: (){
                                                                  setState(() {
                                                                    _isSelectedNawiri=false;
                                                                    _isSelectedUngana=false;
                                                                    _isSelectedTabibu=false;
                                                                    _isSelectedBusiness=true;
                                                                    _tabController.index=1;
                                                                  });
                                                                },
                                                                child: SingleChildScrollView(
                                                                  child: Card(
                                                                    color: _isSelectedBusiness ? Colors.yellow:Colors.white,
                                                                    child: ExpansionTile(
                                                                      title:  Text(data![index].categoryName ?? ''),
                                                                      children: [
                                                                        Text(data[index].description ?? ''),
                                                                        Text(data[index].targetUser ?? ''),
                                                                        Divider(height: 20,),
                                                                        Text("Openning balance of ksh.2000.00,200 USD"),
                                                                        Text("(FX equivalent of USD.200.00"),
                                                                        Text("No minimum balance"),
                                                                        Text("Ledger fee of KES.300.00(USD.5)"),
                                                                        Text("For the first 15 transactions"),
                                                                        Text("and 20.00 for each extra transactions"),
                                                                        Text("Available in KES,USD,GBP and EURO"),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(height: 5,),
                                                              GestureDetector(
                                                                onTap: (){
                                                                  setState(() {
                                                                    _isSelectedNawiri=false;
                                                                    _isSelectedUngana=false;
                                                                    _isSelectedTabibu=true;
                                                                    _isSelectedBusiness=false;
                                                                    _tabController.index=1;
                                                                  });
                                                                },
                                                                child: SingleChildScrollView(
                                                                  child: Card(
                                                                    color: _isSelectedTabibu ? Colors.yellow:Colors.white,
                                                                    child: ExpansionTile(
                                                                      title:  Text(data[index].categoryName ?? ''),
                                                                      children: [
                                                                        Text(data[index].description ?? ''),
                                                                        Text(data[index].targetUser ?? ''),
                                                                        Text("Sidian Bank in partnership with Mediacal Credit Fund"),
                                                                        Text("East Africa provides tailor-made financila solution to.."),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          );
                                                        });
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ))
                            ],
                          ),
                        ),
                        //SizedBox(height: 15,),
                        /*Container(
                          height: 250,
                          width: MediaQuery.of(context).size.width*0.9,
                          child: Stack(
                            children: [
                              Positioned(
                                  top:45,
                                  left: 25,
                                  child: Container(
                                    height: 600,
                                    width: 250,
                                    child: Column(
                                      children: [
                                        Expanded(
                                          child: Scaffold(
                                            body: Center(
                                              child: FutureBuilder<List<RetailModel>>(
                                                future: makeAPICALLCORPORATE(),
                                                builder: (context, snapshot) {
                                                  if (snapshot.connectionState == ConnectionState.waiting) {
                                                    return CircularProgressIndicator();
                                                  } else {
                                                    final data = snapshot.data;
                                                    //print(data);
                                                    return ListView.builder(
                                                        itemCount: data?.length,
                                                        itemBuilder: (context, index) {
                                                          return GestureDetector(
                                                            onTap: (){
                                                              setState(() {
                                                                _isSelectedBusiness=true;
                                                                _isSelectedUngana=false;
                                                                _isSelectedTabibu=false;
                                                                _isSelectedNawiri=false;
                                                                DefaultTabController.of(context)!.animateTo(2);
                                                              });
                                                            },
                                                            child: Card(
                                                              color: _isSelectedBusiness ? Colors.yellow:Colors.white,
                                                              child: Column(
                                                                children: [
                                                                  Text(data![index].id ?? ''),
                                                                  Text(data[index].categoryName ?? ''),
                                                                  Text(data[index].description ?? ''),
                                                                  Text(data[index].targetUser ?? ''),
                                                                ],
                                                              ),
                                                            ),
                                                          );
                                                        });
                                                  }
                                                },
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ))
                            ],
                          ),
                        ),*/
                      ],
                    ),
                  ))
              ),
            ],

          )
        ],
      ),
    );
  }
}


class LoansPage extends StatefulWidget {
  const LoansPage(  {Key? key}) : super(key: key);

  @override
  State<LoansPage> createState() => _LoansPageState();
}

class _LoansPageState extends State<LoansPage> with SingleTickerProviderStateMixin{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController=TabController(length: 3, vsync: this);
  }
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: _tabController.length,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            actions: [
              IconButton(onPressed: (){}, icon: Icon(Icons.search)),
              IconButton(onPressed: (){}, icon: Icon(Icons.more_vert))
            ],
            bottom: ReadOnlyTabBar(
              child: TabBar(
                tabs: [
                  Tab(text: 'Products',),
                  Tab(text: 'KYC'),
                  Tab(text: "Mandates",)
                ],
                controller: _tabController,
              ),
            ),
            title: Text("Loans"),
          ),
          drawer: MenuDrawer(),
          body: TabBarView(
              physics: const NeverScrollableScrollPhysics(),
              controller: _tabController,
              children: [
                RadioButtonPageLoan(),
                KycTabForLoan(),
                MandateLoan(),
              ]
          ),
        )
    );
  }
}
