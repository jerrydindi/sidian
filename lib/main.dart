import 'dart:io';
import 'package:flutter/material.dart';
import 'package:sidian/screens/dashboard.dart';
import 'package:sidian/screens/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Login page',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
    );
  }
}
/*
class LoginPageWork extends StatefulWidget {
  const LoginPageWork({Key? key}) : super(key: key);
  //late LoginRequestModel requestModel;

  @override
  State<LoginPageWork> createState() => _LoginPageWorkState();
}

class _LoginPageWorkState extends State<LoginPageWork> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> globalFormKey = GlobalKey<FormState>();
  //intergate with api
  // late LoginRequestModel requestModel;
  final LocalAuthentication _localAuthentication = LocalAuthentication();
  String _message = "Not Authorized";

  get requestModel => null;

  //get responseModel => null;

  //init state for loginrequestmodel
 // @override
   //void initState(){
   // super.initState();
   // LoginRequestModel requestModel=new LoginRequestModel(email: 'email', password: 'password',);
  //}
  Future<bool> checkingForBioMetrics() async {
    bool canCheckBiometrics = await _localAuthentication.canCheckBiometrics;
    print(canCheckBiometrics);
    return canCheckBiometrics;
  }
  Future<void> _authenticateMe() async {
// 8. this method opens a dialog for fingerprint authentication.
//    we do not need to create a dialog nut it popsup from device natively.
    bool authenticated = false;
    try {
      authenticated = await _localAuthentication.authenticateWithBiometrics(
        localizedReason: "Authenticate for Testing", // message for dialog
        useErrorDialogs: true, // show error in dialog
        stickyAuth: true, // native process
      );
      setState(() {
        _message = authenticated ? "Authorized" : "Not Authorized";
      });
    } catch (e) {
      print(e);
    }
    if (!mounted) return;
  }
  @override
  void initState() {
// TODO: implement initState
    checkingForBioMetrics();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/splashscreen.jpeg'),
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(
                    Colors.lightGreenAccent.withOpacity(0.4),
                    BlendMode.lighten,
                  )
              )
          ),
        ),
        Scaffold(
          key: scaffoldKey,
          backgroundColor: Colors.transparent,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('$_message'),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: Column(
                  children:[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextInput(
                          icon: FontAwesomeIcons.solidEnvelope,
                          hint: "Email",
                          inputType: TextInputType.emailAddress,
                          inputAction: TextInputAction.next,
                        ),
                        PasswordInput(
                          icon: FontAwesomeIcons.lock,
                          hint: "Password",
                          inputAction: TextInputAction.done,
                        ),
                        Column(
                          children: [
                            SizedBox(height:20,),
                            Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Colors.yellowAccent,
                                  borderRadius: BorderRadius.circular(16)
                              ),
                              child: FlatButton(
                                onPressed:(){
                                  if(validateAndSave()){
                                    print(requestModel.toJson());
                                  }
                                  //APIService apiservice=new APIService();
                                  //Navigator.push(context, MaterialPageRoute(builder: (context)=>DashBoard()));
                                },
                                child:Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                                    child: Text('Login',style: lBodyStyle,)),),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            FlatButton(onPressed: (){
                              Navigator.push(context,MaterialPageRoute(builder: (context)=>RecoveryPage()));
                            }, child: Text('Forget Password',style: fBodyStyle,)),
                            //Text("Forget Password?",style: fBodyStyle,),
                            Container(
                              //margin: EdgeInsets.only(top: 10),
                              child: IconButton(
                                onPressed: (){_authenticateMe();},
                                icon:Icon(Icons.fingerprint_rounded,color: Colors.black,size: 50,),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
  //validate form
  bool validateAndSave() {
    final form = globalFormKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    }
    return false;
  }
}
class TextInput extends StatefulWidget {
  const TextInput({
    Key? key,
    required this.icon,
    required this.hint,
    required this.inputAction,
    required this.inputType,
  }) : super(key: key);
  final IconData icon;
  final String hint;
  final TextInputType inputType;
  final TextInputAction inputAction;

  @override
  State<TextInput> createState() => _TextInputState();
}

class _TextInputState extends State<TextInput> {
  late LoginRequestModel requestModel;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    requestModel=new LoginRequestModel(email: 'email', password: 'password');
  }
  @override
  Widget build(BuildContext context) {
   //late LoginRequestModel requestModel;
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 12),
      child: Container(
        child: TextFormField(
          onSaved: (input)=> requestModel.email=input!,
          decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(vertical: 16),
              border: InputBorder.none,
              hintText: widget.hint,
              hintStyle: wBodyStyle,
              prefixIcon: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Icon(
                    widget.icon,
                    color: Colors.black,
                    size: 20,))
          ),
          validator: (input)=>!input!.contains('@')?"Email id should be Valid":null,
          style: yBodyStyle,
          keyboardType: widget.inputType,
          textInputAction: widget.inputAction,
        ),
        decoration: BoxDecoration(
            color: Colors.white10.withOpacity(0.5),borderRadius: BorderRadius.circular(26)
        ),
      ),
    );
  }
}

class PasswordInput extends StatefulWidget {
   PasswordInput({
    Key? key,
    required this.icon,
    required this.hint,
    required this.inputAction,
    //this.inputType,
  }) : super(key: key);
  final IconData icon;
  final String hint;
  //final TextInputType inputType;
  final TextInputAction inputAction;

  @override
  State<PasswordInput> createState() => _PasswordInputState();
}

class _PasswordInputState extends State<PasswordInput> {
  bool hidePassword=true;
  late LoginRequestModel requestModel;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    requestModel=new LoginRequestModel(email: 'email', password: 'password');
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 12),
      child: Container(
        child: TextFormField(
          onSaved: (input)=> requestModel.password=input!,
          decoration: InputDecoration(
              contentPadding: const EdgeInsets.symmetric(vertical: 16),
              border: InputBorder.none,
              hintText: widget.hint,
              hintStyle: wBodyStyle,
              prefixIcon: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Icon(
                    widget.icon,
                    color: Colors.black,
                    size: 20,)),
            suffixIcon: IconButton(onPressed: (){
              setState(() {
                hidePassword=!hidePassword;
              });
            },
              icon: Icon(hidePassword?Icons.visibility_off:Icons.visibility),color: Colors.black,)
          ),
          obscureText: hidePassword,
          validator: (input)=> input!.length < 3?'password should be more that 3 character':null,
          style: yBodyStyle,
          //keyboardType: inputType,
          textInputAction: widget.inputAction,
        ),
        decoration: BoxDecoration(
            color: Colors.white10.withOpacity(0.5),borderRadius: BorderRadius.circular(26)
        ),
      ),
    );
  }
}
*/
// new ocr android only
//int _cameraOcr = FlutterMobileVision.CAMERA_BACK;
//   Future<Null> _read() async {
//     List<OcrText> texts = [];
//     List<StrinKg> values = [];
//     try {
//       texts = await FlutterMobileVision.read(
//         multiple: true,
//         camera: _cameraOcr,
//         waitTap: false,
//       );
//       print(texts);
//       print('bottom ${texts[3].bottom}');
//       print('top ${texts[2].top}');
//       print('left ${texts[4].left}');
//       print('top ${texts[3].right}');
//       print('top ${texts[1].language}');
//       texts.forEach((val) => {
//         values.add(val.value.toString()),
//       });
//       getValue(values);
//       if (!mounted) return;
//     } on Exception {
//       texts.add(new OcrText('Failed to recognize text.'));
//     }
//   }
//
//   getValue(List<String> values) {
//     print(values);
// //You can include logic to handle the values
//   }

/*Container(
child: ListView.builder(
itemCount: 1,
itemBuilder: (context,index){
return Container(
child: Column(
children: [
Container(
height: 230,
child: Stack(
children: [
Positioned(
top: 50,
left: 20,
child: Material(
child: Container(
height: 250.0,
width: MediaQuery.of(context).size.width*0.9,
decoration: BoxDecoration(
color: Colors.white,
borderRadius: BorderRadius.only(
topRight: Radius.circular(30),
topLeft: Radius.circular(30),
bottomRight: Radius.circular(30),
bottomLeft: Radius.circular(30)
),
boxShadow: [
BoxShadow(
color: Colors.grey.withOpacity(0.3),
offset: Offset(-10.0, 10),
blurRadius: 20.0,
spreadRadius: 4.0,
)
]
),
),
)
),
Positioned(
top: 55,
left: 25,
child: Container(
height: 230,
width: 250,
child: Column(
children: [
GestureDetector(
onTap: (){},
child: Card(
child: Row(
mainAxisAlignment: MainAxisAlignment.spaceEvenly,
children: [
Text('General Motors Weekly Check-in'),
IconButton(onPressed: (){}, icon: Icon(Icons.arrow_drop_down)),
ListTile(
title: Text('General Motors Weekly check-in',style: TextStyle(color: Colors.black,fontSize: 10,fontWeight: FontWeight.bold),),
leading: IconButton(onPressed: (){}, icon: Icon(Icons.arrow_drop_down,color: Colors.black,)),
),
Divider(
height: 5,
),
Container(
child: Row(
children: [
Text('10.00am'),
Text('12.00pm'),
Text('4 feb 2021'),
Text('Madina Headquater')
],
),
),
SizedBox(height: 5,),
Container(
child: ListTile(
title: Text('Agenda'),
),
),
],
),
),
),
],
),
)
),
],
),
),
SizedBox(height: 20,),
Container(
height: 230,
child: Stack(
children: [
Positioned(
top: 50,
left: 20,
child: Material(
child: Container(
height: 250.0,
width: MediaQuery.of(context).size.width*0.9,
decoration: BoxDecoration(
color: Colors.white,
borderRadius: BorderRadius.only(
topRight: Radius.circular(30),
topLeft: Radius.circular(30),
bottomRight: Radius.circular(30),
bottomLeft: Radius.circular(30)
),
boxShadow: [
BoxShadow(
color: Colors.grey.withOpacity(0.3),
offset: Offset(-10.0, 10),
blurRadius: 20.0,
spreadRadius: 4.0,
)
]
),
),
)
),
Positioned(
top: 55,
left: 25,
child: Container(
height: 230,
width: 250,
child: Column(
children: [
Row(
mainAxisAlignment: MainAxisAlignment.spaceEvenly,
children: [
Text('Kenya Airport Weekly check-in',style: TextStyle(fontSize: 10,color: Colors.black,fontWeight: FontWeight.bold),),
SizedBox(width: MediaQuery.of(context).size.width*0.1,),
IconButton(onPressed: (){}, icon: Icon(Icons.arrow_drop_up,color: Colors.black,),)
],
),
Container(
child: TextFormField(
//enabled: false,
decoration: InputDecoration(
hintText: '11.00am-2.00pm 4 feb 2022 at KM Headquaters',
border: OutlineInputBorder(),
),
),
decoration: BoxDecoration(
borderRadius: BorderRadius.circular(26)
),
),
Container(
child: Column(
children: [
Text('Agenda'),
TextFormField(
//enabled: false,
minLines: 2,
maxLines: 5,
keyboardType: TextInputType.multiline,
decoration: InputDecoration(
hintText:"Oversea flight to USA",
border: OutlineInputBorder(),
),
),
],
),
decoration: BoxDecoration(
borderRadius: BorderRadius.circular(26)
),
)
],
),
)
),
],
),
),
SizedBox(height: 20,),
Container(
child: FloatingActionButton(onPressed: (){
//Navigator.of(context).pop(TimePopUpScreen());
//TimePopUpScreen();
Navigator.push(context, MaterialPageRoute(builder: (context)=>TimePopUpScreen()));
},child: Icon(Icons.add),backgroundColor: Colors.green,),
)
],
),
);
}),
),*/