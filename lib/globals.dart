import 'package:flutter/material.dart';
import 'dart:convert';
//import 'package:http/http.dart' as http;
import 'package:flutter/rendering.dart';
import 'dart:io';
import 'dart:ui';

String BASE_URL="http://18.185.221.96:8900/channelinterface_dfa/req";
//String email="";
//String password="";
String userEmail="useremail@gmail.com";
//String username="esb";
//String password="esb@ao2021";
//String channel="APP";
PageController pageController = PageController();



Future<String> apiRequest(String url, Map jsonMap) async {
  HttpClient httpClient = new HttpClient();
  HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
  request.headers.set('content-type', 'application/json');
  request.add(utf8.encode(json.encode(jsonMap)));
  HttpClientResponse response = await request.close();
  // todo - you should check the response.statusCode
  String reply = await response.transform(utf8.decoder).join();
  httpClient.close();
  return reply;
}


