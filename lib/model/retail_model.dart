import 'dart:convert';

RetailModel retailModelFromJson(String str) => RetailModel.fromJson(json.decode(str));

String retailModelToJson(RetailModel data) => json.encode(data.toJson());

class RetailModel {
  RetailModel({
    this.id,
    this.targetUser,
     this.categoryName,
     this.description,
     this.createdAt,
  });

  String? id;
  String? targetUser;
  String? categoryName;
  String? description;
  String? createdAt;

  factory RetailModel.fromJson(Map<String, dynamic> json) => RetailModel(
    id: json["id"],
    targetUser: json["target_user"],
    categoryName: json["category_name"],
    description: json["description"],
    createdAt: json["createdAt"],
  );

 Map<String, dynamic> toJson() => {
    "id": id,
    "target_user": targetUser,
    "category_name": categoryName,
    "description": description,
    "createdAt": createdAt,
  };
}