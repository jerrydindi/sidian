import 'dart:convert';

CorporateModel corporateModelFromJson(String str) => CorporateModel.fromJson(json.decode(str));

String corporateModelToJson(CorporateModel data) => json.encode(data.toJson());

class CorporateModel {
  CorporateModel({
    this.id,
    this.categoryName,
    this.targetUser,
    this.description,
    this.createdAt,
  });

  String? id;
  String? targetUser;
  String? categoryName;
  String? description;
  String? createdAt;

  factory CorporateModel.fromJson(Map<String, dynamic> json) => CorporateModel(
    id: json["id"],
    targetUser: json["target_user"],
    categoryName: json["category_name"],
    description: json["description"],
    createdAt: json["createdAt"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "target_user": targetUser,
    "category_name": categoryName,
    "description": description,
    "createdAt": createdAt,
  };
}