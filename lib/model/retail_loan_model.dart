import 'dart:convert';

RetailLoanModel retailLoanModelFromJson(String str) => RetailLoanModel.fromJson(json.decode(str));

String retailLoanModelToJson(RetailLoanModel data) => json.encode(data.toJson());

class RetailLoanModel {
  RetailLoanModel({
    this.id,
    this.targetUser,
    this.categoryName,
    this.description,
    this.createdAt,
  });

  String? id;
  String? targetUser;
  String? categoryName;
  String? description;
  String? createdAt;

  factory RetailLoanModel.fromJson(Map<String, dynamic> json) => RetailLoanModel(
    id: json["id"],
    targetUser: json["target_user"],
    categoryName: json["category_name"],
    description: json["description"],
    createdAt: json["createdAt"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "target_user": targetUser,
    "category_name": categoryName,
    "description": description,
    "createdAt": createdAt,
  };
}