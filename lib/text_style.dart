import 'package:flutter/material.dart';
import 'package:cupertino_icons/cupertino_icons.dart';
import 'package:flutter/painting.dart';

const TextStyle yBodyStyle=TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.bold,
  color: Colors.black,
);
const TextStyle fBodyStyle=TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.bold,
  decoration: TextDecoration.underline,
  color: Colors.white,
);
const TextStyle lBodyStyle=TextStyle(
  fontSize: 22,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);
const TextStyle wBodyStyle=TextStyle(
  fontSize: 24,
  fontWeight: FontWeight.bold,
  color: Colors.black26,
);
